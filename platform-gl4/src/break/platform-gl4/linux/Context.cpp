#if OS_LINUX

#include <assert.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include <break/platform-hal/Event.h>
#include <break/platform-hal/linux/Window.h>

#include "break/platform-gl4/IContext.h"

#include <mn/Memory.h>

#include <new>

namespace brk::platform::gl4
{
	using namespace mn;

	//xlib helpers
	using glXCreateContextAttribsARBProc = GLXContext (*)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
	using glXSwapIntervalEXTProc = void (*)(Display* display, GLXDrawable drawable, int interval);

	struct Context final: IContext
	{
		::Display* display;
		::GLXContext context;
		bool glew_inited;

		void init() override
		{
			display = ::XOpenDisplay(NULL);
			assert(display != NULL && "XOpenDisplay failed");

			const int visual_attribs[] = {
				GLX_X_RENDERABLE        , True,
				GLX_DRAWABLE_TYPE       , GLX_WINDOW_BIT,
				GLX_DOUBLEBUFFER        , True,
				GLX_RENDER_TYPE         , GLX_RGBA_BIT,
				GLX_X_VISUAL_TYPE       , GLX_TRUE_COLOR,
				GLX_RED_SIZE            , 8,
				GLX_GREEN_SIZE          , 8,
				GLX_BLUE_SIZE           , 8,
				GLX_ALPHA_SIZE          , 8,
				GLX_DEPTH_SIZE          , 24,
				GLX_STENCIL_SIZE        , 8,
				GLX_SAMPLE_BUFFERS_ARB  , True,
				GLX_SAMPLES_ARB         , 2,
				None, None
			};

			int fbcount;
			GLXFBConfig* fbc = glXChooseFBConfig(display, DefaultScreen(display),
												 visual_attribs, &fbcount);
			assert(fbc && "glXChooseFBConfig failed");

			int best_fbc  = -1,
				worst_fbc = -1,
				best_num_samp = -1,
				worst_num_samp = 999;

			int i;
			for(i = 0; i < fbcount; ++i)
			{
				XVisualInfo* vi = glXGetVisualFromFBConfig(display, fbc[i]);

				if(vi)
				{
					int samp_buf, samples;
					glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
					glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLES, &samples);

					if(best_fbc < 0 || (samp_buf && samples > best_num_samp))
						best_fbc = i, best_num_samp = samples;
					if(worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
						worst_fbc = i, worst_num_samp = samples;
				}
				XFree(vi);
			}

			GLXFBConfig bestFbc = fbc[best_fbc];
			XFree(fbc);

			glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;
			glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((const GLubyte*)"glXCreateContextAttribsARB");
			assert(glXCreateContextAttribsARB != NULL && "glXGetProcAddressARB failed");

			const int major = 4, minor = 0;
			int context_attribs[] = {
				GLX_CONTEXT_MAJOR_VERSION_ARB, major,
				GLX_CONTEXT_MINOR_VERSION_ARB, minor,
				GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
				None
			};

			context = glXCreateContextAttribsARB(display, bestFbc, 0, True, context_attribs);
			assert(context != NULL && "glXCreateContextAttribsARB failed");
			XSync(display, True);
		}

		void dispose() override
		{
			glXDestroyContext(display, context);
			XCloseDisplay(display);
		}

		void window_init(Window handle) override
		{
			linux::Window* window = (linux::Window*)handle;
			window->display = ::XOpenDisplay(NULL);
			assert(window->display != NULL && "XOpenDisplay failed");

			const int visual_attribs[] = {
				GLX_X_RENDERABLE        , True,
				GLX_DRAWABLE_TYPE       , GLX_WINDOW_BIT,
				GLX_DOUBLEBUFFER        , True,
				GLX_RENDER_TYPE         , GLX_RGBA_BIT,
				GLX_X_VISUAL_TYPE       , GLX_TRUE_COLOR,
				GLX_RED_SIZE            , 8,
				GLX_GREEN_SIZE          , 8,
				GLX_BLUE_SIZE           , 8,
				GLX_ALPHA_SIZE          , 8,
				GLX_DEPTH_SIZE          , 24,
				GLX_STENCIL_SIZE        , 8,
				GLX_SAMPLE_BUFFERS_ARB  , True,
				GLX_SAMPLES_ARB         , 2,
				None
			};

			int fbcount;
			GLXFBConfig* fbc = glXChooseFBConfig(window->display, DefaultScreen(window->display),
												 visual_attribs, &fbcount);
			assert(fbc && "glXChooseFBConfig failed");

			int best_fbc  = -1,
				worst_fbc = -1,
				best_num_samp = -1,
				worst_num_samp = 999;

			int i;
			for(i = 0; i < fbcount; ++i)
			{
				XVisualInfo* vi = glXGetVisualFromFBConfig(window->display, fbc[i]);

				if(vi)
				{
					int samp_buf, samples;
					glXGetFBConfigAttrib(window->display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
					glXGetFBConfigAttrib(window->display, fbc[i], GLX_SAMPLES, &samples);

					if(best_fbc < 0 || (samp_buf && samples > best_num_samp))
						best_fbc = i, best_num_samp = samples;
					if(worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
						worst_fbc = i, worst_num_samp = samples;
				}
				XFree(vi);
			}

			GLXFBConfig bestFbc = fbc[best_fbc];
			XFree(fbc);

			XVisualInfo *vi = glXGetVisualFromFBConfig(window->display, bestFbc);
			XSetWindowAttributes swa;
			Colormap color_map;
			swa.colormap = color_map = XCreateColormap(window->display,
													   RootWindow(window->display, vi->screen),
													   vi->visual, AllocNone);
			swa.background_pixmap = None;
			swa.border_pixel = 0;
			swa.event_mask = StructureNotifyMask;
			
			window->handle = ::XCreateWindow(window->display,
										   RootWindow(window->display, vi->screen),
										   100, 100,
										   window->width, window->height,
										   0,
										   vi->depth,
										   InputOutput,
										   vi->visual,
										   CWBorderPixel | CWColormap | CWEventMask,
										   &swa);
			assert(window->handle && "XCreateWindow failed");
			XFree(vi);

			XSelectInput(window->display, window->handle,
						 StructureNotifyMask |
						 KeyPressMask |
						 KeyReleaseMask |
						 ButtonPressMask |
						 ButtonReleaseMask |
						 PointerMotionMask);

			XMapWindow(window->display, window->handle);
			//set the window title
			XStoreName(window->display, window->handle, window->title);
			XSetIconName(window->display, window->handle, window->title);

			//set delete atom
			XSetWMProtocols(window->display, window->handle, linux::_wm_delete_window(window->display), 1);

			bool result = glXMakeCurrent(window->display, window->handle, context);
			assert(result && "glXMakeCurrent failed");

			if(glew_inited == false)
			{
				glew_inited = glewInit() == GLEW_OK;
				assert(glew_inited && "glewInit failed");
			}

			glEnable(GL_DEPTH_TEST);

			glXSwapIntervalEXTProc swap_interval = nullptr;
			swap_interval = (glXSwapIntervalEXTProc)glXGetProcAddressARB((const GLubyte*)"glXSwapIntervalEXT");
			assert(swap_interval != NULL && "glXGetProcAddressARB failed");

			//disable vsync
			swap_interval(window->display, window->handle, 0);

			//reset the current context
			glXMakeCurrent(window->display, 0, 0);

			window->inited = true;
		}

		void window_bind(Window handle) override
		{
			linux::Window* window = (linux::Window*)handle;
			assert(window->inited && "Window is not inited, consider calling gfx_context_window_init");
			bool result = glXMakeCurrent(window->display, window->handle, this->context);
			assert(result && "glXMakeCurrent failed");
		}

		void window_present(Window handle) override
		{
			linux::Window* window = (linux::Window*)handle;
			glXSwapBuffers(window->display, window->handle);
		}

		void window_unbind() override
		{
			bool result = glXMakeCurrent(this->display, None, this->context);
			assert(result && "glXMakeCurrent failed");
		}
	};

	IContext*
	context_new()
	{
		Context* context = alloc<Context>();
		::new (context) Context();
		return context;
	}

	void
	context_free(IContext* self)
	{
		free(self);
	}
}

#endif