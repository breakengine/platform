#include "break/platform-gl4/Graphics.h"

#include <GL/glew.h>

#include <algorithm>

namespace brk::platform::gl4
{
	using namespace mn;

	struct Handle
	{
		enum KIND
		{
			KIND_NONE,
			KIND_PIPELINE,
			KIND_BUFFER,
			KIND_TEXTURE2D,
			KIND_GEOMETRY,
			KIND_PROGRAM,
			KIND_SAMPLER
		};

		KIND kind;
		union
		{
			struct
			{
				Color clear_color;
			} pipeline;

			struct
			{
				GLuint id;
				GLenum type;
				GLenum usage;
				size_t size;
			} buffer;

			struct
			{
				GLuint id;
				GLenum format;
				size_t width;
				size_t height;
			} texture2d;

			struct
			{
				GLuint id;
				FILTER filter;
				ADDRESS_MODE s_address_mode;
				ADDRESS_MODE t_address_mode;
				ADDRESS_MODE r_address_mode;
				Color border_color;
			} sampler;

			struct
			{
				GLuint id;
			} program;

			struct
			{
				GLuint id;
				Buffer_Layout layout;
			} geometry;
		};
	};

	struct Request
	{
		enum KIND
		{
			KIND_NONE,
			KIND_WINDOW_INIT,
			KIND_WINDOW_DISPOSE, //not used for now but here for complete-ness sake
			KIND_PIPELINE_INIT, //not used for now but here for complete-ness sake
			KIND_PIPELINE_DISPOSE,
			KIND_BUFFER_INIT,
			KIND_BUFFER_DISPOSE,
			KIND_TEXTURE2D_INIT,
			KIND_TEXTURE2D_DISPOSE,
			KIND_SAMPLER_INIT,
			KIND_SAMPLER_DISPOSE,
			KIND_PROGRAM_INIT,
			KIND_PROGRAM_DISPOSE,
			KIND_GEOMETRY_INIT,
			KIND_GEOMETRY_DISPOSE
		};

		KIND kind;
		union
		{
			struct
			{
				Window window;
			} window_init;

			struct
			{
				Handle* handle;
			} pipeline_free;

			struct
			{
				Handle* handle;
				BUFFER_TYPE type;
				USAGE usage;
				Block data;
			} buffer_init;

			struct
			{
				Handle* handle;
			} buffer_dispose;

			struct
			{
				Handle* handle;
				size_t width;
				size_t height;
				Block data;
				USAGE usage;
				PIXEL_FORMAT format;
			} texture2d_init;

			struct
			{
				Handle* handle;
			} texture2d_dispose;

			struct
			{
				Handle* handle;
				FILTER filter;
				ADDRESS_MODE u, v, w;
				Color border;
			} sampler_init;

			struct
			{
				Handle* handle;
			} sampler_dispose;

			struct
			{
				Handle* handle;
				mn::Str vs;
				mn::Str ps;
				Input_Layout layout;
			} program_init;

			struct
			{
				Handle* handle;
			} program_dispose;

			struct
			{
				Handle* handle;
			} geometry_init;

			struct
			{
				Handle* handle;
			} geometry_dispose;
		};
	};

	inline static Request
	_request_window_init(Window window)
	{
		Request self{};
		self.kind = Request::KIND_WINDOW_INIT;
		self.window_init.window = window;
		return self;
	}

	inline static Request
	_request_pipeline_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_PIPELINE_DISPOSE;
		self.pipeline_free.handle = handle;
		return self;
	}

	inline static Request
	_request_buffer_init(Handle* handle, BUFFER_TYPE type, USAGE usage, const Block& data)
	{
		Request self{};
		self.kind = Request::KIND_BUFFER_INIT;
		self.buffer_init.handle = handle;
		self.buffer_init.type = type;
		self.buffer_init.usage = usage;
		self.buffer_init.data = data;
		return self;
	}

	inline static Request
	_request_buffer_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_BUFFER_DISPOSE;
		self.buffer_dispose.handle = handle;
		return self;
	}

	inline static Request
	_request_texture2d_init(Handle* handle, uint32_t width, uint32_t height,
						   const Block& data, USAGE usage, PIXEL_FORMAT format)
	{
		Request self{};
		self.kind = Request::KIND_TEXTURE2D_INIT;
		self.texture2d_init.handle = handle;
		self.texture2d_init.width = width;
		self.texture2d_init.height = height;
		self.texture2d_init.data = data;
		self.texture2d_init.usage = usage;
		self.texture2d_init.format = format;
		return self;
	}

	inline static Request
	_request_texture2d_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_TEXTURE2D_DISPOSE;
		self.texture2d_dispose.handle = handle;
		return self;
	}

	inline static Request
	_request_sampler_init(Handle* handle, const FILTER& filter, const ADDRESS_MODE& u, const ADDRESS_MODE& v, const ADDRESS_MODE& w, const Color& border)
	{
		Request self{};
		self.kind = Request::KIND_SAMPLER_INIT;
		self.sampler_init.filter = filter;
		self.sampler_init.u = u;
		self.sampler_init.v = v;
		self.sampler_init.w = w;
		self.sampler_init.border = border;
		return self;
	}

	inline static Request
	_request_sampler_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_SAMPLER_DISPOSE;
		self.sampler_dispose.handle = handle;
		return self;
	}

	inline static Request
	_request_program_init(Handle* handle, const mn::Str& vs, const mn::Str& ps, const Input_Layout& layout)
	{
		Request self{};
		self.kind = Request::KIND_PROGRAM_INIT;
		self.program_init.handle = handle;
		self.program_init.vs = vs;
		self.program_init.ps = ps;
		self.program_init.layout = layout;
		return self;
	}

	inline static Request
	_request_program_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_PROGRAM_DISPOSE;
		self.program_dispose.handle = handle;
		return self;
	}

	inline static Request
	_request_geometry_init(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_GEOMETRY_INIT;
		self.geometry_init.handle = handle;
		return self;
	}

	inline static Request
	_request_geometry_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_GEOMETRY_DISPOSE;
		self.geometry_dispose.handle = handle;
		return self;
	}

	//helper functions

	//Graphics Helper Functions
	inline static Handle*
	_handle_new(Graphics* self)
	{
		mutex_lock(self->handle_pool_mtx);
			Handle* res = (Handle*)pool_get(self->handle_pool);
		mutex_unlock(self->handle_pool_mtx);
		*res = Handle{};
		return res;
	}

	inline static void
	_handle_free(Graphics* self, Handle* handle)
	{
		mutex_lock(self->handle_pool_mtx);
			pool_put(self->handle_pool, handle);
		mutex_unlock(self->handle_pool_mtx);
	}

	inline static void
	_request_push(Graphics* self, const Request& req)
	{
		mutex_lock(self->req_mtx);
			buf_push(self->req_public, req);
		mutex_unlock(self->req_mtx);
	}


	//OpenGL Helper Functions
	inline static bool
	_check()
	{
		GLenum err = glGetError();
		switch(err)
		{
			case GL_INVALID_ENUM:
				assert(false && "invalid enum value was passed");
				return false;

			case GL_INVALID_VALUE:
				assert(false && "invalid value was passed");
				return false;

			case GL_INVALID_OPERATION:
				assert(false && "invalid operation at the current state of opengl");
				return false;

			case GL_INVALID_FRAMEBUFFER_OPERATION:
				assert(false && "invalid framebuffer operation");
				return false;

			case GL_OUT_OF_MEMORY:
				assert(false && "out of memory");
				return false;

			case GL_STACK_UNDERFLOW:
				assert(false && "stack underflow");
				return false;

			case GL_STACK_OVERFLOW:
				assert(false && "stack overflow");
				return false;

			case GL_NO_ERROR:
			default:
				return true;
		}
	}

	inline static GLenum
	_map(CLEAR_FLAGS flags)
	{
		GLenum res = 0;

		if(flags & CLEAR_FLAGS_COLOR)
			res |= GL_COLOR_BUFFER_BIT;

		if(flags & CLEAR_FLAGS_DEPTH)
			res |= GL_DEPTH_BUFFER_BIT;

		return res;
	}

	inline static GLenum
	_map(BUFFER_TYPE type)
	{
		GLenum res = 0;
		switch(type)
		{
			case BUFFER_TYPE::VERTEX: res = GL_ARRAY_BUFFER; break;
			case BUFFER_TYPE::INDEX: res = GL_ELEMENT_ARRAY_BUFFER; break;
			case BUFFER_TYPE::UNIFORM: res = GL_UNIFORM_BUFFER; break;
			default: assert(false && "unreachable"); break;
		}
		return res;
	}

	inline static GLenum
	_map(USAGE usage)
	{
		GLenum res = 0;
		switch(usage)
		{
			case USAGE::STATIC: res = GL_STATIC_DRAW; break;
			case USAGE::DYNAMIC: res = GL_DYNAMIC_DRAW; break;
			default: assert(false && "unreachable"); break;
		}
		return res;
	}

	inline static GLenum
	_map(PIXEL_FORMAT format)
	{
		GLenum res{};
		switch (format)
		{
			case PIXEL_FORMAT::RGBA:
				res = GL_RGBA;
				break;
				
			case PIXEL_FORMAT::RGB:
				res = GL_RGB;
				break;

			default:
				break;
		}
		return res;
	}

	inline static GLenum
	_map(FILTER filter)
	{
		GLenum res{};
		switch (filter)
		{
			case FILTER::POINT:
				res = GL_NEAREST;
				break;

			case FILTER::LINEAR:
				res = GL_LINEAR;
				break;

			default:
				break;
		}
		return res;
	}

	inline static GLenum
	_map(ADDRESS_MODE address)
	{
		GLenum res{};
		switch (address)
		{
			case ADDRESS_MODE::WRAP:
				res = GL_REPEAT;
				break;

			case ADDRESS_MODE::CLAMP:
				res = GL_CLAMP_TO_EDGE;
				break;

			case ADDRESS_MODE::MIRROR:
				res = GL_MIRRORED_REPEAT;
				break;

			case ADDRESS_MODE::BORDER:
				res = GL_CLAMP_TO_BORDER;
				break;

			default:
				break;
		}
		return res;
	}

	//Request Processing Code
	inline static void
	_request_window_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_WINDOW_INIT);
		const auto&[window] = req.window_init;

		self->context->window_init(window);
	}

	inline static void
	_request_pipeline_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PIPELINE_DISPOSE);

		const auto&[handle] = req.pipeline_free;

		_handle_free(self, handle);
	}

	inline static void
	_request_buffer_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_BUFFER_INIT);

		const auto&[handle, type, usage, data] = req.buffer_init;

		glGenBuffers(1, &handle->buffer.id);

		handle->buffer.type = _map(type);
		handle->buffer.usage = _map(usage);
		handle->buffer.size = data.size;

		assert(data && "data shouldn't be empty");

		glBindBuffer(handle->buffer.type, handle->buffer.id);
		glBufferData(handle->buffer.type, data.size, data.ptr, handle->buffer.usage);
		glBindBuffer(handle->buffer.type, NULL);
	}

	inline static void
	_request_buffer_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_BUFFER_DISPOSE);
		const auto&[handle] = req.buffer_dispose;

		glDeleteBuffers(1, &handle->buffer.id);
		_handle_free(self, handle);
	}

	inline static void
	_request_texture2d_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_TEXTURE2D_INIT);
		const auto&[handle, width, height, data, usage, format] = req.texture2d_init;

		glGenTextures(1, &handle->texture2d.id);
		handle->texture2d.format = _map(format);
		handle->texture2d.width = width;
		handle->texture2d.height = height;
		
		assert(data && "data shouldn't be empty");

		glBindTexture(GL_TEXTURE_2D, handle->texture2d.id);
		glTexImage2D(GL_TEXTURE_2D,
					 0, handle->texture2d.format,
					 width, height,
					 0, handle->texture2d.format,
					 GL_UNSIGNED_BYTE, data.ptr);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	inline static void
	_request_texture2d_free_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_TEXTURE2D_DISPOSE);
		Handle* handle = req.texture2d_dispose.handle;
		glDeleteTextures(1, &handle->texture2d.id);
		_handle_free(self, handle);
	}

	inline static void
	_request_sampler_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_SAMPLER_INIT);
		const auto&[handle, filter, s, t, r, border] = req.sampler_init;
		handle->sampler.filter = filter;
		handle->sampler.s_address_mode = s;
		handle->sampler.t_address_mode = t;
		handle->sampler.r_address_mode = r;
		handle->sampler.border_color = border;
		GLfloat border_color[4] = { border[0], border[1], border[2], border[3] };

		glGenSamplers(1, &handle->sampler.id);
		glSamplerParameteri(handle->sampler.id, GL_TEXTURE_MIN_FILTER, _map(filter));
		glSamplerParameteri(handle->sampler.id, GL_TEXTURE_MAG_FILTER, _map(filter));
		glSamplerParameteri(handle->sampler.id, GL_TEXTURE_WRAP_S, _map(s));
		glSamplerParameteri(handle->sampler.id, GL_TEXTURE_WRAP_T, _map(t));
		glSamplerParameteri(handle->sampler.id, GL_TEXTURE_WRAP_R, _map(r));
		glSamplerParameterfv(handle->sampler.id, GL_TEXTURE_BORDER_COLOR, border_color);
	}

	inline static void
	_request_sampler_free_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_SAMPLER_DISPOSE);
		Handle* handle = req.sampler_dispose.handle;
		glDeleteSamplers(1, &handle->sampler.id);
		_handle_free(self, handle);
	}

	inline static void
	_request_program_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PROGRAM_INIT);
		const auto& [handle, vs, ps, layout] = req.program_init;

		// vertex shader
		auto vso = glCreateShader(GL_VERTEX_SHADER);
		const GLchar* vsrc[1]{ vs.ptr };
		GLint vlength[1]{ vs.count };
		glShaderSource(vso, 1, vsrc, vlength);
		glCompileShader(vso);
		GLint vsuccess;
		glGetShaderiv(vso, GL_COMPILE_STATUS, &vsuccess);
		if (!vsuccess)
		{
			assert(false && "failed to compile vertex shader");
		}

		// frag shader
		auto pso = glCreateShader(GL_FRAGMENT_SHADER);
		const GLchar* psrc[1]{ ps.ptr };
		GLint plength[1]{ ps.count };
		glShaderSource(pso, 1, psrc, plength);
		glCompileShader(pso);
		GLint psuccess;
		glGetShaderiv(pso, GL_COMPILE_STATUS, &psuccess);
		if (!psuccess)
		{
			assert(false && "failed to compile pixel shader");
		}

		// program
		handle->program.id = glCreateProgram();
		auto program = handle->program.id;
		glAttachShader(program, vso);
		glAttachShader(program, pso);
		glLinkProgram(program);
		glUseProgram(program);
		glDeleteShader(vso);
		glDeleteShader(pso);
	}

	inline static void
	_request_program_free_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PROGRAM_DISPOSE);
		glDeleteProgram(req.program_dispose.handle->program.id);
	}

	inline static void
	_request_geometry_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_GEOMETRY_INIT);
		Handle* handle = req.geometry_init.handle;
		glGenVertexArrays(1, &handle->geometry.id);
		glBindVertexArray(handle->geometry.id);

		size_t count = handle->geometry.layout.count;
		Input_Buffer* attributes = handle->geometry.layout.buffers;
		for (size_t index = 0; index < count; ++index)
		{
			Handle* handle = (Handle*)attributes[index].buffer;
			glBindBuffer(handle->buffer.type, handle->buffer.id);
			glEnableVertexAttribArray(index);
			glVertexAttribPointer(index, attributes[index].element_count, GL_FLOAT, attributes[index].normalized, attributes[index].stride, (void*)attributes[index].offset);
			glDisableVertexAttribArray(index);
		}
	}

	inline static void
	_request_geometry_free_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_GEOMETRY_DISPOSE);
		Handle* handle = req.geometry_dispose.handle;
		size_t count = handle->geometry.layout.count;
		for (size_t index = 0; index < count; ++index)
		{
			Handle* buffer = (Handle*)handle->geometry.layout.buffers[index].buffer;
			glDeleteBuffers(1, &buffer->buffer.id);
			_handle_free(self, buffer);
		}
		glDeleteVertexArrays(1, &handle->geometry.id);
		_handle_free(self, handle);
	}

	inline static void
	_request_process_all(Graphics* self)
	{
		mutex_lock(self->req_mtx);
			std::swap(self->req_private, self->req_public);
		mutex_unlock(self->req_mtx);

		for(const Request& req : self->req_private)
		{
			switch(req.kind)
			{
				case Request::KIND_WINDOW_INIT:
					_request_window_init_run(self, req);
					break;

				case Request::KIND_WINDOW_DISPOSE:
					assert(false && "unimplmented");
					break;

				case Request::KIND_PIPELINE_DISPOSE:
					_request_pipeline_dispose_run(self, req);
					break;

				case Request::KIND_BUFFER_INIT:
					_request_buffer_init_run(self, req);
					break;

				case Request::KIND_BUFFER_DISPOSE:
					_request_buffer_dispose_run(self, req);
					break;

				case Request::KIND_TEXTURE2D_INIT:
					_request_texture2d_init_run(self, req);
					break;

				case Request::KIND_TEXTURE2D_DISPOSE:
					_request_texture2d_free_run(self, req);
					break;

				case Request::KIND_SAMPLER_INIT:
					_request_sampler_init_run(self, req);
					break;

				case Request::KIND_SAMPLER_DISPOSE:
					_request_sampler_free_run(self, req);
					break;

				case Request::KIND_PROGRAM_INIT:
					_request_program_init_run(self, req);
					break;

				case Request::KIND_PROGRAM_DISPOSE:
					_request_program_free_run(self, req);
					break;

				case Request::KIND_GEOMETRY_INIT:
					_request_geometry_init_run(self, req);
					break;

				case Request::KIND_GEOMETRY_DISPOSE:
					_request_geometry_free_run(self, req);
					break;

				default:
					assert(false && "unreachable");
					break;
			}
		}

		buf_clear(self->req_private);
	}


	//Cmd Processing Functions
	inline static void
	_cmd_window_use_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Window window = (Window)cmd_pop_ptr(cmd, ix);
		self->context->window_bind(window);
		self->state.window = window;
	}

	inline static void
	_cmd_window_present_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		assert(self->state.window);
		self->context->window_present(self->state.window);
	}

	inline static void
	_cmd_pipeline_use_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* h = (Handle*)cmd_pop_ptr(cmd, ix);
		assert(h->kind == Handle::KIND_PIPELINE);

		glClearColor(h->pipeline.clear_color.x,
					 h->pipeline.clear_color.y,
					 h->pipeline.clear_color.z,
					 h->pipeline.clear_color.w);

		self->state.pipeline = h;
	}

	inline static void
	_cmd_clear_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		CLEAR_FLAGS flags = (CLEAR_FLAGS)cmd_pop8(cmd, ix);
		glClear(_map(flags));
	}

	inline static void
	_cmd_clear_color_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Color color = cmd_pop_color(cmd, ix);
		glClearColor(color[0], color[1], color[2], color[3]);
	}

	inline static void
	_cmd_clear_depth_stencil_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		float depth = cmd_pop32f(cmd, ix);
		uint8_t stencil = cmd_pop8(cmd, ix);
		glClearDepthf(depth);
		glClearStencil(stencil);
	}

	inline static void
	_cmd_program_use_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* handle = (Handle*)cmd_pop_ptr(cmd, ix);
		glUseProgram(handle->program.id);
	}

	inline static void
	_cmd_buffer_map_write_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* handle = (Handle*) cmd_pop_ptr(cmd, ix);
		assert(handle->kind == Handle::KIND_BUFFER);

		Block block = cmd_pop_block(cmd, ix);
		assert(handle->buffer.size == block.size && 
			"cmd_buffer_map_write require the block to be the same size as the buffer");

		glBindBuffer(handle->buffer.type, handle->buffer.id);
		void* buffer = glMapBuffer(handle->buffer.type, GL_WRITE_ONLY);
		::memcpy(buffer, block.ptr, block.size);
		glUnmapBuffer(handle->buffer.type);
		glBindBuffer(handle->buffer.type, 0);
	}

	inline static void
	_cmd_run(Graphics* self, const Cmd& cmd)
	{
		size_t ix = 0;
		while(ix < cmd.bytes.count)
		{
			ISA opcode = cmd_pop_opcode(cmd, ix);
			switch(opcode)
			{
				case ISA::WINDOW_USE:
					_cmd_window_use_run(self, cmd, ix);
					break;

				case ISA::WINDOW_PRESENT:
					_cmd_window_present_run(self, cmd, ix);
					break;

				case ISA::PIPELINE_USE:
					_cmd_pipeline_use_run(self, cmd, ix);
					break;

				case ISA::CLEAR:
					_cmd_clear_run(self, cmd, ix);
					break;

				case ISA::CLEAR_COLOR:
					_cmd_clear_color_run(self, cmd, ix);
					break;

				case ISA::CLEAR_DEPTH_STENCIL:
					_cmd_clear_depth_stencil_run(self, cmd, ix);
					break;

				case ISA::PROGRAM_USE:
					_cmd_program_use_run(self, cmd, ix);
					break;

				case ISA::BUFFER_MAP_WRITE:
					_cmd_buffer_map_write_run(self, cmd, ix);
					break;

				case ISA::IGL:
				default:
					assert(false && "invalid instruction");
					break;
			}
		}
	}

	inline static void
	_cmd_process_all(Graphics* self)
	{
		//get the private stuff
		mutex_lock(self->cmd_buf_mtx);
			std::swap(self->cmd_buf_private, self->cmd_buf_public);
		mutex_unlock(self->cmd_buf_mtx);

		//sort the keys
		std::sort(buf_begin(self->cmd_buf_private), buf_end(self->cmd_buf_private),
				  [](const Cmd& a, const Cmd& b) { return a.sort_key < b.sort_key; });

		for(Cmd& cmd : self->cmd_buf_private)
		{
			_cmd_run(self, cmd);
			self->cmd_discard(cmd);
		}

		//clear the buffer
		buf_clear(self->cmd_buf_private);
	}

	inline static void
	_reset_state(Graphics* self)
	{
		self->state = State{};

		self->context->window_unbind();
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	}

	void
	Graphics::init()
	{
		this->context = context_new();
		this->context->init();

		this->handle_pool = pool_new(sizeof(Handle), 128);
		this->handle_pool_mtx = mutex_new("GL4 Handle Mutex");

		this->cmd_reuse = buf_new<Cmd>();
		this->cmd_reuse_mtx = mutex_new("GL4 Cmd Reuse Mutex");

		this->cmd_buf_public = buf_new<Cmd>();
		this->cmd_buf_private = buf_new<Cmd>();
		this->cmd_buf_mtx = mutex_new("GL4 Cmd Mutex");

		this->req_public = buf_new<Request>();
		this->req_private = buf_new<Request>();
		this->req_mtx = mutex_new("GL4 Requests Mutex");

		this->state = State{};
	}

	void
	Graphics::dispose()
	{
		pool_free(this->handle_pool);
		mutex_free(this->handle_pool_mtx);

		destruct(this->cmd_reuse);
		mutex_free(this->cmd_reuse_mtx);

		buf_free(this->cmd_buf_public);
		buf_free(this->cmd_buf_private);
		mutex_free(this->cmd_buf_mtx);

		buf_free(this->req_public);
		buf_free(this->req_private);
		mutex_free(this->req_mtx);

		this->context->dispose();
		context_free(this->context);
	}

	//Window
	void
	Graphics::window_init(Window window)
	{
		mutex_lock(this->req_mtx);
			buf_push(this->req_public, _request_window_init(window));
		mutex_unlock(this->req_mtx);
	}

	void
	Graphics::window_dispose(Window window)
	{
		//do nothing
	}

	//Cmd
	Cmd
	Graphics::cmd_new()
	{
		Cmd res = platform::cmd_new();
		mutex_lock(this->cmd_reuse_mtx);
			if(this->cmd_reuse.count > 0)
			{
				res = buf_top(this->cmd_reuse);
				buf_pop(this->cmd_reuse);
			}
		mutex_unlock(this->cmd_reuse_mtx);
		return res;
	}

	void
	Graphics::cmd_discard(Cmd& cmd)
	{
		cmd_clear_bytes(cmd);
		mutex_lock(this->cmd_reuse_mtx);
			buf_push(this->cmd_reuse, cmd);
		mutex_unlock(this->cmd_reuse_mtx);
		cmd = Cmd{};
	}

	void
	Graphics::cmd_submit(Cmd& cmd, int sort_key)
	{
		cmd.sort_key = sort_key;
		mutex_lock(this->cmd_buf_mtx);
			buf_push(this->cmd_buf_public, cmd);
		mutex_unlock(this->cmd_buf_mtx);
		cmd = Cmd{};
	}

	void
	Graphics::cmd_flush()
	{
		_request_process_all(this);
		_cmd_process_all(this);
		#if DEBUG
			_reset_state(this);
		#endif
	}

	//Pipeline
	Pipeline
	Graphics::pipeline_new(const Input_Layout& layout)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_PIPELINE;

		h->pipeline.clear_color = Color{};
		return (Pipeline)h;
	}

	void
	Graphics::pipeline_free(Pipeline handle)
	{
		_request_push(this, _request_pipeline_dispose((Handle*)handle));
	}

	void
	Graphics::pipeline_clearcolor(Pipeline pipeline, const Color& c)
	{
		Handle* h = (Handle*)pipeline;
		assert(h->kind == Handle::KIND_PIPELINE);
		h->pipeline.clear_color = c;
	}


	//Buffer
	Buffer
	Graphics::buffer_new(BUFFER_TYPE type, USAGE usage, const Block& data)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_BUFFER;

		_request_push(this, _request_buffer_init(h, type, usage, data));
		return (Buffer)h;
	}

	void
	Graphics::buffer_free(Buffer buffer)
	{
		_request_push(this, _request_buffer_dispose((Handle*)buffer));
	}


	//Texture2D
	Texture2D
	Graphics::texture2d_new(uint32_t width, uint32_t height,
							const Block& data, USAGE usage, PIXEL_FORMAT format)
	{
		Handle* handle = _handle_new(this);
		handle->kind = Handle::KIND_TEXTURE2D;
		handle->texture2d.width = width;
		handle->texture2d.height = height;
		_request_push(this, _request_texture2d_init(handle, width, height, data, usage, format));
		return (Texture2D)handle;
	}

	void
	Graphics::texture2d_free(Texture2D texture)
	{
		_request_push(this, _request_texture2d_dispose((Handle*)texture));
	}


	//Sampler
	Sampler
	Graphics::sampler_new(FILTER filter,
						  ADDRESS_MODE u, ADDRESS_MODE v, ADDRESS_MODE w,
						  const Color& border)
	{
		Handle* handle = _handle_new(this);
		handle->kind = Handle::KIND_SAMPLER;

		_request_push(this, _request_sampler_init(handle, filter, u, v, w, border));
		return (Sampler)handle;
	}

	void
	Graphics::sampler_free(Sampler handle)
	{
		_request_push(this, _request_sampler_dispose((Handle*)handle));
	}


	//Program
	Program
	Graphics::program_new(const Input_Layout& layout, const mn::Str& vs, const mn::Str& ps)
	{
		Handle* handle = _handle_new(this);
		handle->kind = Handle::KIND_PROGRAM;

		_request_push(this, _request_program_init(handle, vs, ps, layout));
		return (Program)handle;
	}

	void
	Graphics::program_free(Program program)
	{
		_request_push(this, _request_program_dispose((Handle*)program));
	}


	//Geometry
	Geometry
	Graphics::geometry_new(const Buffer_Layout& layout)
	{
		Handle* handle = _handle_new(this);
		handle->kind = Handle::KIND_GEOMETRY;
		handle->geometry.layout = layout;

		_request_push(this, _request_geometry_init(handle));
		return (Geometry)handle;
	}

	void
	Graphics::geometry_free(Geometry geometry)
	{
		_request_push(this, _request_geometry_dispose((Handle*)geometry));
	}
}
