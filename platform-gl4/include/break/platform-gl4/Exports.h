#pragma once

#if defined(OS_WINDOWS)
	#if defined(PLT_GL4_DLL)
		#define API_PLT_GL4 __declspec(dllexport)
	#else
		#define API_PLT_GL4 __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_PLT_GL4 
#endif