#pragma once

#include "break/platform-gl4/Exports.h"
#include "break/platform-gl4/IContext.h"

#include <break/platform-hal/IGraphics.h>

#include <mn/Pool.h>
#include <mn/Thread.h>
#include <mn/Buf.h>

namespace brk::platform::gl4
{
	struct Request;
	struct Handle;

	struct State
	{
		Window window;
		Handle* pipeline;
	};

	struct Graphics final : IGraphics
	{
		IContext* context;

		mn::Pool handle_pool;
		mn::Mutex handle_pool_mtx;

		mn::Buf<Cmd> cmd_reuse;
		mn::Mutex cmd_reuse_mtx;

		mn::Buf<Cmd> cmd_buf_public;
		mn::Buf<Cmd> cmd_buf_private;
		mn::Mutex cmd_buf_mtx;

		mn::Buf<Request> req_public;
		mn::Buf<Request> req_private;
		mn::Mutex req_mtx;

		//State
		State state;

		//Graphics stuff
		API_PLT_GL4 void
		init() override;

		API_PLT_GL4 void
		dispose() override;


		//Window
		API_PLT_GL4 void
		window_init(Window window) override;

		API_PLT_GL4 void
		window_dispose(Window window) override;


		//Cmd
		API_PLT_GL4 Cmd
		cmd_new() override;

		API_PLT_GL4 void
		cmd_discard(Cmd& cmd) override;

		API_PLT_GL4 void
		cmd_submit(Cmd& cmd, int sort_key) override;

		API_PLT_GL4 void
		cmd_flush() override;


		//Pipeline
		API_PLT_GL4 Pipeline
		pipeline_new(const Input_Layout& layout) override;

		API_PLT_GL4 void
		pipeline_free(Pipeline pipeline) override;

		API_PLT_GL4 void
		pipeline_clearcolor(Pipeline pipeline, const Color& c) override;


		//Buffer
		API_PLT_GL4 Buffer
		buffer_new(BUFFER_TYPE type, USAGE usage, const mn::Block& data) override;

		API_PLT_GL4 void
		buffer_free(Buffer buffer) override;


		//Texture
		API_PLT_GL4 Texture2D
		texture2d_new(uint32_t width, uint32_t height,
					  const mn::Block& data, USAGE usage, PIXEL_FORMAT format) override;

		API_PLT_GL4 void
		texture2d_free(Texture2D texture) override;


		//Sampler
		API_PLT_GL4 Sampler
		sampler_new(FILTER filter, ADDRESS_MODE u, ADDRESS_MODE v, ADDRESS_MODE w, const Color& border) override;

		API_PLT_GL4 void
		sampler_free(Sampler handle) override;


		//Program
		API_PLT_GL4 Program
		program_new(const Input_Layout& layout, const mn::Str& vs, const mn::Str& ps) override;

		API_PLT_GL4 void
		program_free(Program program) override;


		//Geometry
		API_PLT_GL4 Geometry
		geometry_new(const Buffer_Layout& layout) override;

		API_PLT_GL4 void
		geometry_free(Geometry geometry) override;
	};
}
