#pragma once

#include "break/platform-gl4/Exports.h"

#include <mn/Base.h>

namespace brk::platform
{
	MS_FWD_HANDLE(Window);
}

namespace brk::platform::gl4
{
	struct IContext
	{
		virtual void init() = 0;
		virtual void dispose() = 0;
		virtual void window_init(Window window) = 0;
		virtual void window_bind(Window window) = 0;
		virtual void window_present(Window window) = 0;
		virtual void window_unbind() = 0;
	};

	API_PLT_GL4 IContext*
	context_new();

	API_PLT_GL4 void
	context_free(IContext* context);
}
