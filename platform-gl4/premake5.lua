glew = path.getabsolute("../external/glew")
include "../external/glew"

project "platform-gl4"
	language "C++"
	kind "SharedLib"

	files
	{
		"include/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"include/",
		"%{mn}/include",
		"%{hml}/include",
		"%{glew}/include",
		"../platform-hal/include"
	}

	links
	{
		"mn",
		"glew",
		"platform-hal"
	}

	--language configuration
	warnings "Extra"
	cppdialect "c++17"
	systemversion "latest"

	--linux configuration
	filter "system:linux"
		defines { "OS_LINUX" }
		links { "X11", "GL", "GLU" }

	--windows configuration
	filter "system:windows"
		defines { "OS_WINDOWS" }
		links { "opengl32" }

	--os agnostic configuration
	filter "configurations:debug"
		targetsuffix "d"
		defines {"DEBUG", "PLT_GL4_DLL"}
		symbols "On"

	filter "configurations:release"
		defines {"NDEBUG", "PLT_GL4_DLL"}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"