mn = path.getabsolute("external/mn")
hamilton = path.getabsolute("external/hamilton")

workspace "platform"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	startproject "scratch"
	defaultplatform "x64"

	group "External"
		include "external/mn/mn"
		include "external/hamilton/hml"

	group ""

	include "platform-hal"
	include "platform-gfx"
	include "scratch"