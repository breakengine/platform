#if OS_LINUX
#include "break/platform-gfx/Graphics.h"

#include <break/platform-gl4/Graphics.h>

#include <mn/Memory.h>

#include <new>

namespace brk::platform
{
	using mn::alloc;
	using mn::free;

	Graphics
	graphics_new(GRAPHICS_BACKEND backend)
	{
		switch(backend)
		{
			case GRAPHICS_BACKEND::GL4:
			{
				gl4::Graphics* self = alloc<gl4::Graphics>();
				::new (self) gl4::Graphics();
				self->init();
				return (Graphics)self;
			}

			default:
				return nullptr;
		}
	}
}

#endif
