#include "break/platform-gfx/Graphics.h"

#include <break/platform-hal/IGraphics.h>

#include <mn/Memory.h>

namespace brk::platform
{
	using namespace mn;

	void
	graphics_free(Graphics graphics)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->dispose();
		free(self);
	}


	//Window
	void
	graphics_window_init(Graphics graphics, Window window)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->window_init(window);
	}

	void
	graphics_window_dispose(Graphics graphics, Window window)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->window_dispose(window);
	}


	//Cmd
	Cmd
	graphics_cmd_new(Graphics graphics)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->cmd_new();
	}

	void
	graphics_cmd_discard(Graphics graphics, Cmd& cmd)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->cmd_discard(cmd);
	}

	void
	graphics_cmd_submit(Graphics graphics, Cmd& cmd, int sort_key)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->cmd_submit(cmd, sort_key);
	}

	void
	graphics_cmd_flush(Graphics graphics)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->cmd_flush();
	}


	//Pipeline
	Pipeline
	graphics_pipeline_new(Graphics graphics, const Input_Layout& layout)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->pipeline_new(layout);
	}

	void
	graphics_pipeline_free(Graphics graphics, Pipeline pipeline)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->pipeline_free(pipeline);
	}

	void
	graphics_pipeline_clearcolor(Graphics graphics, Pipeline pipeline, const Color& c)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->pipeline_clearcolor(pipeline, c);
	}


	//Buffer
	Buffer
	graphics_buffer_new(Graphics graphics, BUFFER_TYPE type, USAGE usage, const Block& data)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->buffer_new(type, usage, data);
	}

	void
	graphics_buffer_free(Graphics graphics, Buffer buffer)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->buffer_free(buffer);
	}


	//Texture2D
	Texture2D
	graphics_texture2d_new(Graphics graphics, uint32_t width, uint32_t height,
		const Block& data, USAGE usage, PIXEL_FORMAT format)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->texture2d_new(width, height, data, usage, format);
	}

	void
	graphics_texture2d_free(Graphics graphics, Texture2D texture)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->texture2d_free(texture);
	}


	//Sampler
	Sampler
	graphics_sampler_new(Graphics graphics, FILTER filter,
		ADDRESS_MODE u, ADDRESS_MODE v, ADDRESS_MODE w, const Color& border)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->sampler_new(filter, u, v, w, border);
	}

	void
	graphics_sampler_free(Graphics graphics, Sampler sampler)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->sampler_free(sampler);
	}


	//Program
	Program
	graphics_program_new(Graphics graphics, const Input_Layout& layout, const Str& vs,
		const Str& ps)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->program_new(layout, vs, ps);
	}

	void
	graphics_program_free(Graphics graphics, Program program)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->program_free(program);
	}


	//Geometry
	Geometry
	graphics_geometry_new(Graphics graphics, const Buffer_Layout& layout)
	{
		IGraphics* self = (IGraphics*)graphics;
		return self->geometry_new(layout);
	}

	void
	graphics_geometry_free(Graphics graphics, Geometry geometry)
	{
		IGraphics* self = (IGraphics*)graphics;
		self->geometry_free(geometry);
	}
}
