if os.ishost("windows") then
	include "../platform-dx11"
	include "../platform-gl4"
elseif os.ishost("linux") then
	include "../platform-gl4"
end

project "platform-gfx"
	language "C++"
	kind "SharedLib"

	files
	{
		"include/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"include/",
		"%{mn}/include",
		"%{hml}/include",
		"../platform-hal/include"
	}

	links
	{
		"mn",
		"glew",
		"platform-hal"
	}

	--language configuration
	warnings "Extra"
	cppdialect "c++17"
	systemversion "latest"

	--linux configuration
	filter "system:linux"
		defines { "OS_LINUX" }
		includedirs { "../platform-gl4/include" }
		links { "platform-gl4" }

	--windows configuration
	filter "system:windows"
		defines { "OS_WINDOWS" }
		includedirs
		{
			"../platform-gl4/include",
			"../platform-dx11/include"
		}
		links
		{
			"platform-gl4",
			"platform-dx11"
		}

	--os agnostic configuration
	filter "configurations:debug"
		targetsuffix "d"
		defines {"DEBUG", "PLT_GFX_DLL"}
		symbols "On"

	filter "configurations:release"
		defines {"NDEBUG", "PLT_GFX_DLL"}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"