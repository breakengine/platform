#pragma once

#include "break/platform-gfx/Exports.h"
#include "break/platform-gfx/Graphics_Backend.h"

#include <break/platform-hal/ISA.h>
#include <break/platform-hal/Cmd.h>

#include <mn/Base.h>
#include <mn/Str.h>

namespace brk::platform
{
	MS_HANDLE(Graphics);

	API_PLT_GFX Graphics
	graphics_new(GRAPHICS_BACKEND backend);

	API_PLT_GFX void
	graphics_free(Graphics graphics);

	inline static void
	destruct(Graphics graphics)
	{
		graphics_free(graphics);
	}


	//Window stuff
	MS_FWD_HANDLE(Window);

	API_PLT_GFX void
	graphics_window_init(Graphics graphics, Window window);

	API_PLT_GFX void
	graphics_window_dispose(Graphics graphics, Window window);


	//Cmd Stuff
	API_PLT_GFX Cmd
	graphics_cmd_new(Graphics graphics);

	API_PLT_GFX void
	graphics_cmd_discard(Graphics graphics, Cmd& cmd);

	API_PLT_GFX void
	graphics_cmd_submit(Graphics graphics, Cmd& cmd, int sort_key);

	API_PLT_GFX void
	graphics_cmd_flush(Graphics graphics);


	//Pipeline Stuff
	MS_HANDLE(Pipeline);

	API_PLT_GFX Pipeline
	graphics_pipeline_new(Graphics graphics, const Input_Layout& layout);

	API_PLT_GFX void
	graphics_pipeline_free(Graphics graphics, Pipeline pipeline);

	API_PLT_GFX void
	graphics_pipeline_clearcolor(Graphics graphics, Pipeline pipeline, const Color& c);


	//Buffer Stuff
	MS_HANDLE(Buffer);

	API_PLT_GFX Buffer
	graphics_buffer_new(Graphics graphics, BUFFER_TYPE type, USAGE usage, const mn::Block& data);

	API_PLT_GFX void
	graphics_buffer_free(Graphics graphics, Buffer buffer);


	//Texture2D Stuff
	MS_HANDLE(Texture2D);

	API_PLT_GFX Texture2D
	graphics_texture2d_new(Graphics graphics, uint32_t width, uint32_t height,
		const mn::Block& data, USAGE usage, PIXEL_FORMAT format);

	API_PLT_GFX void
	graphics_texture2d_free(Graphics graphics, Texture2D texture);


	//Sampler Stuff
	MS_HANDLE(Sampler);

	API_PLT_GFX Sampler
	graphics_sampler_new(Graphics graphics, FILTER filter,
		ADDRESS_MODE u, ADDRESS_MODE v, ADDRESS_MODE w, const Color& border);

	API_PLT_GFX void
	graphics_sampler_free(Graphics graphics, Sampler sampler);


	//Program Stuff
	MS_HANDLE(Program);

	API_PLT_GFX Program
	graphics_program_new(Graphics graphics, const Input_Layout& layout, const mn::Str& vs,
		const mn::Str& ps);

	API_PLT_GFX void
	graphics_program_free(Graphics graphics, Program program);


	//Geometry Stuff
	MS_HANDLE(Geometry);

	API_PLT_GFX Geometry
	graphics_geometry_new(Graphics graphics, const Buffer_Layout& layout);

	API_PLT_GFX void
	graphics_geometry_free(Graphics graphics, Geometry geometry);
}
