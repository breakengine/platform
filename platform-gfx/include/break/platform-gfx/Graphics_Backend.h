#pragma once

namespace brk::platform
{
	enum class GRAPHICS_BACKEND
	{
		GL4,
		DX11
	};
}