#pragma once

#if defined(OS_WINDOWS)
	#if defined(PLT_GFX_DLL)
		#define API_PLT_GFX __declspec(dllexport)
	#else
		#define API_PLT_GFX __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_PLT_GFX 
#endif