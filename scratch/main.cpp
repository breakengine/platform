#include <mn/IO.h>
#include <mn/OS.h>

#include <break/platform-hal/Window.h>
#include <break/platform-gfx/Graphics.h>

#include <hml/Vector.h>

using namespace mn;
using namespace hml;
using namespace brk::platform;

int main(int argc, char** argv)
{
	//allocator_push(leak_detector());
	printfmt("Hello, World!\n");

	//create a graphics with the suitable backend
	Graphics gfx = graphics_new(GRAPHICS_BACKEND::DX11);
	if(gfx == nullptr)
	{
		printfmt("Can't make the graphics object\n");
	}
	//create a window
	Window win = window_new(800, 600, "Break Engine");

	//prepare the window for the graphics rendering
	graphics_window_init(gfx, win);


	//setup the pipeline and commands
	Color clearcolor{ 1.0f, 0.0f, 0.0f, 1.0f };
	float step = 0.001;
	Pipeline p = graphics_pipeline_new(gfx, Input_Layout{});
	graphics_pipeline_clearcolor(gfx, p, clearcolor);

	vec4f positions[] = {
		{0.0f, 0.0f, 0.0f, 0.0f},
		{0.0f, 0.0f, 0.0f, 0.0f},
		{0.0f, 0.0f, 0.0f, 0.0f}
	};


	//setup the buffer
	Buffer vertex_buf = graphics_buffer_new(gfx, BUFFER_TYPE::VERTEX, USAGE::STATIC, block_from(positions));

	while(true)
	{
		//get an event from the window
		Window_Event event = window_poll(win);

		//if the user pressed the X button on the window then close the loop
		if(event.kind == Window_Event::KIND_WINDOW_CLOSE)
			break;

		if(event.kind == Window_Event::KIND_KEYBOARD_KEY &&
		   event.keyboard.key == KEYBOARD::ESC)
			break;


		clearcolor.y += step;
		if(clearcolor.y > 1.0f || clearcolor.y < 0.0f)
			step *= -1;
		graphics_pipeline_clearcolor(gfx, p, clearcolor);

		Cmd cmd = graphics_cmd_new(gfx);
			cmd_window_use(cmd, win);
			cmd_pipeline_use(cmd, p);
			cmd_clear(cmd, CLEAR_FLAGS_COLOR | CLEAR_FLAGS_DEPTH);
			cmd_present(cmd);
		graphics_cmd_submit(gfx, cmd, 0);
		graphics_cmd_flush(gfx);
	}

	//free the stuff
	graphics_buffer_free(gfx, vertex_buf);
	graphics_window_dispose(gfx, win);
	//we flush here to force it to process the free requests above
	graphics_cmd_flush(gfx);

	graphics_pipeline_free(gfx, p);

	//don't forget to free the window
	window_free(win);
	//and the graphics
	graphics_free(gfx);

	return 0;
}