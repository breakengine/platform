project "scratch"
	language "C++"
	kind "ConsoleApp"

	files 
	{
		"**.cpp",
		"**.h"
	}

	includedirs
	{
		"%{mn}/include",
		"%{hamilton}/include",
		"../platform-hal/include",
		"../platform-gfx/include"
	}

	links
	{
		"mn",
		"platform-hal",
		"platform-gfx"
	}

	cppdialect "c++17"
	systemversion "latest"

	filter "system:linux"
		defines { "OS_LINUX" }

	filter "system:windows"
		defines { "OS_WINDOWS" }