project "platform-dx11"
	language "C++"
	kind "SharedLib"

	files
	{
		"include/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"include/",
		"%{mn}/include",
		"%{hml}/include",
		"../platform-hal/include"
	}

	links
	{
		"mn",
		"platform-hal"
	}

	--language configuration
	warnings "Extra"
	cppdialect "c++17"
	systemversion "latest"

	--windows configuration
	filter "system:windows"
		defines { "OS_WINDOWS" }
		links { "d3d11", "dxgi", "d3dcompiler" }

	--os agnostic configuration
	filter "configurations:debug"
		targetsuffix "d"
		defines {"DEBUG", "PLT_DX11_DLL"}
		symbols "On"

	filter "configurations:release"
		defines {"NDEBUG", "PLT_DX11_DLL"}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"