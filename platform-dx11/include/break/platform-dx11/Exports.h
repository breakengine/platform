#pragma once

#if defined(OS_WINDOWS)
	#if defined(PLT_DX11_DLL)
		#define API_PLT_DX11 __declspec(dllexport)
	#else
		#define API_PLT_DX11 __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_PLT_DX11 
#endif