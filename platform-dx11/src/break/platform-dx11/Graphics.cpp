#include "break/platform-dx11/Graphics.h"

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <windowsx.h>
#undef DELETE

#include <break/platform-hal/Window.h>
#include <break/platform-hal/winos/Window.h>

#include <dxgi.h>
#include <d3d11.h>
#include <d3dcommon.h>
#include <d3dcompiler.h>

#include <mn/IO.h>
#include <mn/Buf.h>

#include <algorithm>

#include <assert.h>

namespace brk::platform::dx11
{
	using namespace mn;

	struct Swap_Chain
	{
		IDXGISwapChain* swap_chain;
		ID3D11RenderTargetView* render_target_view;
		ID3D11Texture2D* depth_buffer;
		ID3D11DepthStencilView* depth_stencil_view;
		D3D11_VIEWPORT viewport;
	};

	struct Handle
	{
		enum KIND
		{
			KIND_NONE,
			KIND_PIPELINE,
			KIND_BUFFER,
			KIND_TEXTURE2D,
			KIND_SAMPLER,
			KIND_PROGRAM,
			KIND_GEOMETRY
		};

		KIND kind;
		union
		{
			struct
			{
				Color clear_color;
				Input_Layout layout;
				ID3D11DepthStencilState* depth_stencil;
				ID3D11RasterizerState*   rasterizer;
				ID3D11BlendState*		 blend;
			} pipeline;

			struct
			{
				ID3D11Buffer* dx;
				BUFFER_TYPE type;
				USAGE usage;
				size_t size;
			} buffer;

			struct
			{
				ID3D11Texture2D* dx;
				ID3D11ShaderResourceView* view;
				uint32_t width, height;
				USAGE usage;
				PIXEL_FORMAT format;
			} texture2d;

			struct
			{
				ID3D11SamplerState* dx;
				FILTER filter;
				ADDRESS_MODE u, v, w;
				Color border;
			} sampler;

			struct
			{
				Input_Layout layout;
				ID3D11VertexShader* dx_vertex_shader;
				ID3D11PixelShader* dx_pixel_shader;
				ID3D11InputLayout* dx_input_layout;
			} program;

			struct
			{
				Buffer_Layout layout;
			} geometry;
		};
	};

	struct Request
	{
		enum KIND
		{
			KIND_NONE,
			KIND_WINDOW_INIT,
			KIND_WINDOW_DISPOSE,
			KIND_PIPELINE_INIT,
			KIND_PIPELINE_DISPOSE,
			KIND_BUFFER_INIT,
			KIND_BUFFER_DISPOSE,
			KIND_TEXTURE2D_INIT,
			KIND_TEXTURE2D_DISPOSE,
			KIND_SAMPLER_INIT,
			KIND_SAMPLER_DISPOSE,
			KIND_PROGRAM_INIT,
			KIND_PROGRAM_DISPOSE,
			KIND_GEOMETRY_INIT, //not used
			KIND_GEOMETRY_DISPOSE
		};

		KIND kind;
		union
		{
			struct
			{
				Window window;
			} window_init;

			struct
			{
				Window window;
			} window_dispose;

			struct
			{
				Handle* handle;
			} pipeline_init;

			struct
			{
				Handle* handle;
			} pipeline_free;

			struct
			{
				Handle* handle;
				Block data;
			} buffer_new;

			struct
			{
				Handle* handle;
			} buffer_free;

			struct
			{
				Handle* handle;
				uint32_t width;
				uint32_t height;
				Block data;
				USAGE usage;
				PIXEL_FORMAT format;
			} texture2d_new;

			struct
			{
				Handle* handle;
			} texture2d_free;

			struct
			{
				Handle* handle;
				FILTER filter;
				ADDRESS_MODE u, v, w;
				Color border;
			} sampler_init;

			struct
			{
				Handle* handle;
			} sampler_dispose;

			struct
			{
				Handle* handle;
				Input_Layout layout;
				Str vertex_shader;
				Str pixel_shader;
			} program_init;

			struct
			{
				Handle* handle;
			} program_dispose;

			struct
			{
				Handle* handle;
			} geometry_dispose;
		};
	};

	inline static Request
	_request_window_init(Window window)
	{
		Request self{};
		self.kind = Request::KIND_WINDOW_INIT;
		self.window_init.window = window;
		return self;
	}

	inline static Request
	_request_window_dispose(Window window)
	{
		Request self{};
		self.kind = Request::KIND_WINDOW_DISPOSE;
		self.window_dispose.window = window;
		return self;
	}

	inline static Request
	_request_pipeline_init(Handle* handle)
	{
		assert(handle->kind == Handle::KIND_PIPELINE);
		Request self{};
		self.kind = Request::KIND_PIPELINE_INIT;
		self.pipeline_init.handle = handle;
		return self;
	}

	inline static Request
	_request_pipeline_dispose(Handle* handle)
	{
		assert(handle->kind == Handle::KIND_PIPELINE);
		Request self{};
		self.kind = Request::KIND_PIPELINE_DISPOSE;
		self.pipeline_free.handle = handle;
		return self;
	}

	inline static Request
	_request_buffer_init(Handle* handle, const Block& data)
	{
		assert(handle->kind == Handle::KIND_BUFFER);
		Request self{};
		self.kind = Request::KIND_BUFFER_INIT;
		self.buffer_new.handle = handle;
		self.buffer_new.data = data;
		return self;
	}

	inline static Request
	_request_buffer_dispose(Handle* handle)
	{
		assert(handle->kind == Handle::KIND_BUFFER);
		Request self{};
		self.kind = Request::KIND_BUFFER_DISPOSE;
		self.buffer_free.handle = handle;
		return self;
	}

	inline static Request
	_request_texture2d_init(Handle* handle,
							uint32_t width, uint32_t height,
							const Block& data, USAGE usage, PIXEL_FORMAT format)
	{
		Request self{};
		self.kind = Request::KIND_TEXTURE2D_INIT;
		self.texture2d_new.handle = handle;
		self.texture2d_new.width = width;
		self.texture2d_new.height = height;
		self.texture2d_new.data = data;
		self.texture2d_new.usage = usage;
		self.texture2d_new.format = format;
		return self;
	}

	inline static Request
	_request_texture2d_dispose(Handle* handle)
	{
		assert(handle->kind == Handle::KIND_TEXTURE2D);
		Request self{};
		self.kind = Request::KIND_TEXTURE2D_DISPOSE;
		self.texture2d_free.handle = handle;
		return self;
	}

	inline static Request
	_request_sampler_init(Handle* handle, FILTER filter,
						  ADDRESS_MODE u, ADDRESS_MODE v, ADDRESS_MODE w, const Color& border)
	{
		Request self{};
		self.kind = Request::KIND_SAMPLER_INIT;
		self.sampler_init.handle = handle;
		self.sampler_init.filter = filter;
		self.sampler_init.u = u;
		self.sampler_init.v = v;
		self.sampler_init.w = w;
		self.sampler_init.border = border;
		return self;
	}

	inline static Request
	_request_sampler_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_SAMPLER_DISPOSE;
		self.buffer_free.handle = handle;
		return self;
	}

	inline static Request
	_request_program_init(Handle* handle, const Input_Layout& layout, const Str& vs, const Str& ps)
	{
		Request self{};
		self.kind = Request::KIND_PROGRAM_INIT;
		self.program_init.handle = handle;
		self.program_init.layout = layout;
		self.program_init.vertex_shader = vs;
		self.program_init.pixel_shader = ps;
		return self;
	}

	inline static Request
	_request_program_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_PROGRAM_DISPOSE;
		self.program_dispose.handle = handle;
		return self;
	}

	inline static Request
	_request_goemetry_dispose(Handle* handle)
	{
		Request self{};
		self.kind = Request::KIND_GEOMETRY_DISPOSE;
		self.geometry_dispose.handle = handle;
		return self;
	}

	inline static Swap_Chain*
	_swap_chain_new(Graphics* gfx, HWND window, int width, int height)
	{
		Swap_Chain* self = alloc_zerod<Swap_Chain>();

		//create the swap chain
		{
			DXGI_SWAP_CHAIN_DESC desc;
			ZeroMemory(&desc, sizeof(desc));

			//set to a single back buffer + 1 front buffer = Double Buffer
			desc.BufferCount = 1;
			//set swap chain size
			desc.BufferDesc.Width = width;
			desc.BufferDesc.Height = height;
			//set pixel format
			desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			//disable the vsync for now
			desc.BufferDesc.RefreshRate.Numerator = 0;
			desc.BufferDesc.RefreshRate.Denominator = 1;
			//set the usage of the back buffer
			desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			//set the window handle to which this swap chain is attached
			desc.OutputWindow = window;
			//2x multisampling like opengl
			desc.SampleDesc.Count = 2;
			desc.SampleDesc.Quality = 0;
			//make it windowed for now
			desc.Windowed = true;
			//set to unspecified since we have no preference in those stuff
			desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			//discard the back buffer contents after presenting
			desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			//don't set any advanced flags
			desc.Flags = 0;

			//create the swap chain
			HRESULT res = gfx->factory->CreateSwapChain(gfx->device, &desc, &self->swap_chain);
			assert(SUCCEEDED(res) && "IDXGIFactory::CreateSwapChain failed");
		}

		//create the render target
		{
			//get the back-buffer to create the render target
			ID3D11Texture2D* back_buffer = nullptr;
			HRESULT res = self->swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&back_buffer);
			assert(SUCCEEDED(res) && "IDXGISwapChain::GetBuffer failed");

			//create render target
			res = gfx->device->CreateRenderTargetView(back_buffer, NULL, &self->render_target_view);
			assert(SUCCEEDED(res) && "ID3D11Device::CreateRenderTargetView failed");

			//release to decrement the reference count
			back_buffer->Release();
			back_buffer = nullptr;
		}

		//create the depth buffer texture
		{
			//init the depth buffer
			D3D11_TEXTURE2D_DESC desc;
			ZeroMemory(&desc, sizeof(desc));
			desc.Width = width;
			desc.Height = height;
			desc.MipLevels = 1;
			desc.ArraySize = 1;
			desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			desc.SampleDesc.Count = 2;
			desc.SampleDesc.Quality = 0;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = 0;

			HRESULT res = gfx->device->CreateTexture2D(&desc, nullptr, &self->depth_buffer);
			assert(SUCCEEDED(res) && "ID3D11Device::CreateTexture2D failed");
		}

		//create depth stencil view
		{
			D3D11_DEPTH_STENCIL_VIEW_DESC desc;
			ZeroMemory(&desc, sizeof(desc));
			desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			//Note(Moustapha): the MS at the end to indicate the multisampling
			desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
			desc.Texture2D.MipSlice = 0;

			HRESULT res = gfx->device->CreateDepthStencilView(self->depth_buffer, &desc, &self->depth_stencil_view);
			assert(SUCCEEDED(res) && "ID3D11Device::CreateDepthStencilView failed");
		}

		//create viewport
		{
			self->viewport.Width = (float)width;
			self->viewport.Height = (float)height;
			self->viewport.MinDepth = 0.0f;
			self->viewport.MaxDepth = 1.0f;
			self->viewport.TopLeftX = 0.0f;
			self->viewport.TopLeftY = 0.0f;
		}

		return self;
	}

	inline static void
	_swap_chain_free(Swap_Chain* self)
	{
		self->swap_chain->Release();
		self->swap_chain = nullptr;

		self->render_target_view->Release();
		self->render_target_view = nullptr;

		self->depth_buffer->Release();
		self->depth_buffer = nullptr;

		self->depth_stencil_view->Release();
		self->depth_stencil_view = nullptr;

		free(self);
	}

	inline static ID3D11DepthStencilState*
	_depth_stencil_state_new(Graphics* self)
	{
		D3D11_DEPTH_STENCIL_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		desc.DepthEnable = true;
		desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		desc.DepthFunc = D3D11_COMPARISON_LESS;

		desc.StencilEnable = true;
		desc.StencilReadMask = 0xFF;
		desc.StencilWriteMask = 0xFF;

		desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		ID3D11DepthStencilState* depth_stencil_state = nullptr;
		HRESULT res = self->device->CreateDepthStencilState(&desc, &depth_stencil_state);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateDepthStencilState failed");

		return depth_stencil_state;
	}

	inline static void
	_depth_stencil_state_free(ID3D11DepthStencilState*& self)
	{
		self->Release();
		self = nullptr;
	}

	inline static ID3D11RasterizerState*
	_rasterizer_state_new(Graphics* self)
	{
		D3D11_RASTERIZER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		desc.AntialiasedLineEnable = false;
		desc.CullMode = D3D11_CULL_BACK;
		desc.DepthBias = 0;
		desc.DepthBiasClamp = 0.0f;
		desc.DepthClipEnable = true;
		desc.FillMode = D3D11_FILL_SOLID;
		desc.FrontCounterClockwise = true;
		desc.MultisampleEnable = true;
		desc.ScissorEnable = false;
		desc.SlopeScaledDepthBias = 0.0f;

		ID3D11RasterizerState* rasterizer_state = nullptr;
		HRESULT res = self->device->CreateRasterizerState(&desc, &rasterizer_state);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateRasterizerState failed");

		return rasterizer_state;
	}

	inline static void
	_rasterizer_state_free(ID3D11RasterizerState*& self)
	{
		self->Release();
		self = nullptr;
	}

	inline static ID3D11BlendState*
	_blend_state_new(Graphics* self)
	{
		D3D11_BLEND_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		desc.AlphaToCoverageEnable = false;
		desc.IndependentBlendEnable = false;
		desc.RenderTarget[0].BlendEnable = true;
		desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		ID3D11BlendState* blend_state = nullptr;
		HRESULT res = self->device->CreateBlendState(&desc, &blend_state);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateBlendState failed");

		return blend_state;
	}

	inline static void
	_blend_state_free(ID3D11BlendState*& self)
	{
		self->Release();
		self = nullptr;
	}

	inline static DXGI_FORMAT
	_map_format(PIXEL_FORMAT format)
	{
		switch(format)
		{
			//dx doesn't have RGB so we unpack RGB into an opaque RGBA
			case PIXEL_FORMAT::RGB:
			case PIXEL_FORMAT::RGBA:
				return DXGI_FORMAT_R8G8B8A8_UNORM;

			default:
				assert(false && "unreachable");
				return DXGI_FORMAT_R8G8B8A8_UNORM;
		}
	}

	inline static uint32_t
	_map_format_size(PIXEL_FORMAT format)
	{
		switch(format)
		{
			//dx doesn't have RGB so we unpack RGB into an opaque RGBA
			case PIXEL_FORMAT::RGB:
			case PIXEL_FORMAT::RGBA:
				return 16;

			default:
				assert(false && "unreachable");
				return DXGI_FORMAT_R8G8B8A8_UNORM;
		}
	}

	inline static D3D11_FILTER
	_map_filter(FILTER filter)
	{
		switch(filter)
		{
			case FILTER::LINEAR:
				return D3D11_FILTER_MIN_MAG_MIP_LINEAR;

			case FILTER::POINT:
				return D3D11_FILTER_MIN_MAG_MIP_POINT;

			default:
				assert(false && "unreachable");
				return D3D11_FILTER(0);
		}
	}

	inline static D3D11_TEXTURE_ADDRESS_MODE
	_map_address_mode(ADDRESS_MODE mode)
	{
		switch(mode)
		{
			case ADDRESS_MODE::BORDER:
				return D3D11_TEXTURE_ADDRESS_BORDER;

			case ADDRESS_MODE::CLAMP:
				return D3D11_TEXTURE_ADDRESS_CLAMP;

			case ADDRESS_MODE::MIRROR:
				return D3D11_TEXTURE_ADDRESS_MIRROR;

			case ADDRESS_MODE::WRAP:
				return D3D11_TEXTURE_ADDRESS_WRAP;

			default:
				assert(false && "unreachable");
				return D3D11_TEXTURE_ADDRESS_MODE(0);
		}
	}

	inline static const char*
	_map_semantic(SEMANTIC semantic)
	{
		switch(semantic)
		{
			case SEMANTIC::POSITION:
				return "POSITION";

			case SEMANTIC::COLOR:
				return "COLOR";

			default:
				assert(false && "unreachable");
				return "";
		}
	}

	inline static DXGI_FORMAT
	_map_type(INPUT_TYPE type)
	{
		switch(type)
		{
			case INPUT_TYPE::VEC3F:
				return DXGI_FORMAT_R32G32B32_FLOAT;

			case INPUT_TYPE::RGBA:
				return DXGI_FORMAT_R32G32B32A32_FLOAT;

			default:
				assert(false && "unreachable");
				return DXGI_FORMAT(0);
		}
	}

	inline static Block
	_texture2d_unpack(const Block& data, PIXEL_FORMAT format, uint32_t width, uint32_t height)
	{
		//dx doesn't have RGB so we unpack RGB into an opaque RGBA
		if(format == PIXEL_FORMAT::RGBA)
		{
			return data;
		}
		else if(format == PIXEL_FORMAT::RGB)
		{
			Block res = alloc_from(allocator_tmp(), width * height * _map_format_size(format), alignof(int));
			char* src_it = (char*)data.ptr;
			char* dst_it = (char*)res.ptr;
			char* end_it = src_it + data.size;
			while (src_it != end_it)
			{
				//Red
				*dst_it = *src_it;
				++dst_it; ++src_it;
				//Green
				*dst_it = *src_it;
				++dst_it; ++src_it;
				//Blue
				*dst_it = *src_it;
				++dst_it; ++src_it;
				//Alpha
				*dst_it = 255;
				++dst_it;
			}
			return res;
		}
		else
		{
			assert(false && "unreachable");
			return Block{};
		}
	}

	inline static Handle*
	_handle_new(Graphics* self)
	{
		mutex_lock(self->handle_pool_mtx);
			Handle* res = (Handle*)pool_get(self->handle_pool);
		mutex_unlock(self->handle_pool_mtx);
		*res = Handle{};
		return res;
	}

	inline static void
	_handle_free(Graphics* self, Handle* handle)
	{
		mutex_lock(self->handle_pool_mtx);
			pool_put(self->handle_pool, handle);
		mutex_unlock(self->handle_pool_mtx);
	}

	inline static void
	_request_push(Graphics* self, const Request& req)
	{
		mutex_lock(self->req_mtx);
			buf_push(self->req_public, req);
		mutex_unlock(self->req_mtx);
	}


	//requests run
	inline static void
	_request_window_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_WINDOW_INIT);
		const auto&[handle] = req.window_init;

		winos::Window* window = (winos::Window*)handle;
		Swap_Chain* swap_chain = _swap_chain_new(self, window->handle, window->width, window->height);
		window_user_data_set(handle, swap_chain);
	}

	inline static void
	_request_window_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_WINDOW_DISPOSE);
		const auto&[handle] = req.window_dispose;

		Swap_Chain* swap_chain = (Swap_Chain*)window_user_data(handle);
		_swap_chain_free(swap_chain);
		window_user_data_set(handle, nullptr);
	}

	inline static void
	_request_pipeline_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PIPELINE_INIT);
		const auto&[handle] = req.pipeline_init;

		handle->pipeline.depth_stencil = _depth_stencil_state_new(self);
		handle->pipeline.rasterizer = _rasterizer_state_new(self);
		handle->pipeline.blend = _blend_state_new(self);
	}

	inline static void
	_request_pipeline_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PIPELINE_DISPOSE);
		const auto&[handle] = req.pipeline_free;

		_depth_stencil_state_free(handle->pipeline.depth_stencil);
		_rasterizer_state_free(handle->pipeline.rasterizer);
		_blend_state_free(handle->pipeline.blend);
	}

	inline static void
	_request_buffer_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_BUFFER_INIT);
		const auto&[handle, data] = req.buffer_new;

		D3D11_BUFFER_DESC desc;
		desc.ByteWidth = data.size;
		desc.MiscFlags = 0;
		desc.StructureByteStride = 0;

		handle->buffer.size = data.size;

		switch(handle->buffer.type)
		{
			case BUFFER_TYPE::VERTEX:
				desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
				break;

			case BUFFER_TYPE::UNIFORM:
				desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				assert(data.size % 16 == 0 && "uniform buffer size should be 16 byte aligned");
				break;

			default:
				assert(false && "unreachable");
				break;
		}

		switch(handle->buffer.usage)
		{
			case USAGE::DYNAMIC:
				desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
				desc.Usage = D3D11_USAGE_DYNAMIC;
				break;

			case USAGE::STATIC:
				desc.CPUAccessFlags = 0;
				desc.Usage = D3D11_USAGE_IMMUTABLE;
				assert(data &&
					   "you can't set the block to null when creating a static buffer");
				break;

			default:
				assert(false && "unreachable");
				break;
		}

		D3D11_SUBRESOURCE_DATA init_desc;
		init_desc.pSysMem = data.ptr;
		init_desc.SysMemPitch = 0;
		init_desc.SysMemSlicePitch = 0;

		HRESULT res = self->device->CreateBuffer(&desc, &init_desc, &handle->buffer.dx);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateBuffer failed");
	}

	inline static void
	_request_buffer_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_BUFFER_DISPOSE);
		const auto&[handle] = req.buffer_free;

		handle->buffer.dx->Release();
		_handle_free(self, handle);
	}

	inline static void
	_request_texture2d_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_TEXTURE2D_INIT);
		const auto&[handle, width, height, sent_data, usage, format] = req.texture2d_new;

		Block data = _texture2d_unpack(sent_data, format, width, height);

		D3D11_TEXTURE2D_DESC desc{};
		desc.ArraySize = 1;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		desc.MipLevels = 1;
		desc.SampleDesc.Count = 1;
		if(usage == USAGE::DYNAMIC)
		{
			desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			desc.Usage = D3D11_USAGE_DYNAMIC;
		}
		else
		{
			desc.Usage = D3D11_USAGE_IMMUTABLE;
		}
		desc.Width = width;
		desc.Height = height;
		desc.Format = _map_format(format);

		D3D11_SUBRESOURCE_DATA descData{};
		descData.pSysMem = data.ptr;
		descData.SysMemPitch = width * _map_format_size(format);
		descData.SysMemSlicePitch = data.size;
		HRESULT res = self->device->CreateTexture2D(&desc, &descData, &handle->texture2d.dx);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateTexture2D failed");

		D3D11_SHADER_RESOURCE_VIEW_DESC descView{};
		descView.Format = desc.Format;
		descView.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		descView.Texture2D.MipLevels = desc.MipLevels;
		res = self->device->CreateShaderResourceView(handle->texture2d.dx, &descView, &handle->texture2d.view);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateShaderResourceView failed");
	}

	inline static void
	_request_texture2d_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_TEXTURE2D_DISPOSE);
		const auto&[handle] = req.texture2d_free;

		handle->texture2d.dx->Release();
		handle->texture2d.dx = nullptr;

		handle->texture2d.view->Release();
		handle->texture2d.view = nullptr;

		_handle_free(self, handle);
	}

	inline static void
	_request_sampler_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_SAMPLER_INIT);
		const auto&[handle, filter, u, v, w, border] = req.sampler_init;

		D3D11_SAMPLER_DESC desc{};
		desc.Filter = _map_filter(filter);
		desc.AddressU = _map_address_mode(u);
		desc.AddressV = _map_address_mode(v);
		desc.AddressW = _map_address_mode(w);
		desc.MipLODBias = 0;
		desc.MaxAnisotropy = 1;
		desc.ComparisonFunc = D3D11_COMPARISON_LESS;
		desc.BorderColor[0] = border.x;
		desc.BorderColor[1] = border.y;
		desc.BorderColor[2] = border.z;
		desc.BorderColor[3] = border.w;
		desc.MinLOD = 0;
		desc.MaxLOD = D3D11_FLOAT32_MAX;

		HRESULT res = self->device->CreateSamplerState(&desc, &handle->sampler.dx);
		assert(SUCCEEDED(res) && "ID3D11Device::CreateSamplerState failed");
	}

	inline static void
	_request_sampler_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_SAMPLER_DISPOSE);
		const auto&[handle] = req.sampler_dispose;

		assert(handle->kind == Handle::KIND_SAMPLER);
		handle->sampler.dx->Release();
		handle->sampler.dx = nullptr;

		_handle_free(self, handle);
	}

	inline static void
	_request_program_init_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PROGRAM_INIT);
		const auto&[handle, layout, vs, ps] = req.program_init;

		assert(handle->kind == Handle::KIND_PROGRAM);

		ID3D10Blob* err = nullptr;
		ID3D10Blob* vs_ir = nullptr;
		{
			HRESULT res = D3DCompile(vs.ptr, vs.count, NULL, NULL, NULL,
									 "main", "vs_5_0", 0, 0, &vs_ir, &err);
			if(FAILED(res))
			{
				printfmt_err("{}\n", (char*)err->GetBufferPointer());
				assert(false && "failed to compile vertex shader");
			}
			res = self->device->CreateVertexShader(vs_ir->GetBufferPointer(),
												   vs_ir->GetBufferSize(),
												   NULL,
												   &handle->program.dx_vertex_shader);
			assert(SUCCEEDED(res) && "ID3D11Device::CreateVertexShader failed");
		}

		ID3D10Blob* ps_ir = nullptr;
		{
			HRESULT res = D3DCompile(ps.ptr, ps.count, NULL, NULL, NULL,
									 "main", "ps_5_0", 0, 0, &ps_ir, &err);
			if(FAILED(res))
			{
				printfmt_err("{}\n", (char*)err->GetBufferPointer());
				assert(false && "failed to compile vertex shader");
			}
			res = self->device->CreatePixelShader(ps_ir->GetBufferPointer(),
												  ps_ir->GetBufferSize(),
												  NULL,
												  &handle->program.dx_pixel_shader);
			assert(SUCCEEDED(res) && "ID3D11Device::CreatePixelShader failed");
		}

		{
			D3D11_INPUT_ELEMENT_DESC desc[Input_Layout::MAX_ELEMENTS];
			for(size_t i = 0; i < handle->program.layout.count; ++i)
			{
				desc[i].SemanticName = _map_semantic(handle->program.layout[i].semantic);
				desc[i].SemanticIndex = 0;
				desc[i].Format = _map_type(handle->program.layout[i].type);
				desc[i].InputSlot = i;
				desc[i].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
				desc[i].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
				desc[i].InstanceDataStepRate = 0;
			}
			HRESULT res = self->device->CreateInputLayout(desc, handle->program.layout.count,
														  vs_ir->GetBufferPointer(),
														  vs_ir->GetBufferSize(),
														  &handle->program.dx_input_layout);
			assert(SUCCEEDED(res) && "ID3D11Device::CreateInputLayout failed");
		}

		vs_ir->Release();
		vs_ir = nullptr;

		ps_ir->Release();
		ps_ir = nullptr;
	}

	inline static void
	_request_program_dispose_run(Graphics* self, const Request& req)
	{
		assert(req.kind == Request::KIND_PROGRAM_DISPOSE);
		const auto&[handle] = req.program_dispose;

		assert(handle->kind == Handle::KIND_PROGRAM);

		handle->program.dx_vertex_shader->Release();
		handle->program.dx_vertex_shader = nullptr;

		handle->program.dx_pixel_shader->Release();
		handle->program.dx_pixel_shader = nullptr;

		handle->program.dx_input_layout->Release();
		handle->program.dx_input_layout = nullptr;

		_handle_free(self, handle);
	}

	inline static void
	_request_process_all(Graphics* self)
	{
		mutex_lock(self->req_mtx);
			std::swap(self->req_public, self->req_private);
		mutex_unlock(self->req_mtx);

		for(const Request& req : self->req_private)
		{
			switch(req.kind)
			{
				case Request::KIND_WINDOW_INIT:
					_request_window_init_run(self, req);
					break;

				case Request::KIND_WINDOW_DISPOSE:
					_request_window_dispose_run(self, req);
					break;

				case Request::KIND_PIPELINE_INIT:
					_request_pipeline_init_run(self, req);
					break;

				case Request::KIND_PIPELINE_DISPOSE:
					_request_pipeline_dispose_run(self, req);
					break;

				case Request::KIND_BUFFER_INIT:
					_request_buffer_init_run(self, req);
					break;

				case Request::KIND_BUFFER_DISPOSE:
					_request_buffer_dispose_run(self, req);
					break;

				case Request::KIND_TEXTURE2D_INIT:
					_request_texture2d_init_run(self, req);
					break;

				case Request::KIND_TEXTURE2D_DISPOSE:
					_request_texture2d_dispose_run(self, req);
					break;

				case Request::KIND_SAMPLER_INIT:
					_request_sampler_init_run(self, req);
					break;

				case Request::KIND_SAMPLER_DISPOSE:
					_request_sampler_dispose_run(self, req);
					break;

				case Request::KIND_PROGRAM_INIT:
					_request_program_init_run(self, req);
					break;

				case Request::KIND_PROGRAM_DISPOSE:
					_request_program_dispose_run(self, req);
					break;

				default:
					assert(false && "unreachable");
					break;
			}
		}

		buf_clear(self->req_private);
	}

	//cmd run
	inline static void
	_cmd_window_use_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Window window = (Window)cmd_pop_ptr(cmd, ix);
		Swap_Chain* swap_chain = (Swap_Chain*)window_user_data(window);
		self->context->OMSetRenderTargets(1,
										  &swap_chain->render_target_view,
										  swap_chain->depth_stencil_view);
		self->context->RSSetViewports(1, &swap_chain->viewport);

		self->state.window = window;
	}

	inline static void
	_cmd_window_present_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		assert(self->state.window);
		Window window = self->state.window;
		Swap_Chain* swap_chain = (Swap_Chain*)window_user_data(window);
		//no vsync for now
		swap_chain->swap_chain->Present(0, 0);
	}

	inline static void
	_cmd_pipeline_use_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* handle = (Handle*)cmd_pop_ptr(cmd, ix);
		assert(handle->kind == Handle::KIND_PIPELINE);
		self->context->OMSetDepthStencilState(handle->pipeline.depth_stencil, 1);
		self->context->RSSetState(handle->pipeline.rasterizer);
		self->context->OMSetBlendState(handle->pipeline.blend, nullptr, 0xFFFFFF);

		self->state.pipeline = handle;
	}

	inline static void
	_cmd_clear_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		assert(self->state.pipeline != nullptr);
		CLEAR_FLAGS flags = (CLEAR_FLAGS)cmd_pop8(cmd, ix);

		ID3D11DepthStencilView* depth_stencil = nullptr;
		ID3D11RenderTargetView* render_target = nullptr;

		if(self->state.window)
		{
			Swap_Chain* swap_chain = (Swap_Chain*)window_user_data(self->state.window);
			render_target = swap_chain->render_target_view;
			depth_stencil = swap_chain->depth_stencil_view;
		}

		if((flags & CLEAR_FLAGS_COLOR) && render_target)
			self->context->ClearRenderTargetView(render_target,
												 self->state.pipeline->pipeline.clear_color.data);

		if((flags & CLEAR_FLAGS_DEPTH) && depth_stencil)
			self->context->ClearDepthStencilView(depth_stencil, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	inline static void
	_cmd_clear_color_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		assert(self->state.window != nullptr);
		Swap_Chain* swap_chain = (Swap_Chain*)window_user_data(self->state.window);
		Color c = cmd_pop_color(cmd, ix);
		self->context->ClearRenderTargetView(swap_chain->render_target_view, c.data);
	}

	inline static void
	_cmd_clear_depth_stencil_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		assert(self->state.window != nullptr);
		Swap_Chain* swap_chain = (Swap_Chain*)window_user_data(self->state.window);
		float depth = cmd_pop32f(cmd, ix);
		uint8_t stencil = cmd_pop8(cmd, ix);
		self->context->ClearDepthStencilView(swap_chain->depth_stencil_view, D3D11_CLEAR_DEPTH, depth, stencil);
	}

	inline static void
	_cmd_program_use_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* h = (Handle*)cmd_pop_ptr(cmd, ix);
		assert(h->kind == Handle::KIND_PROGRAM);
		assert(self->state.pipeline && "you must use a pipeline first");
		
		self->context->IASetInputLayout(h->program.dx_input_layout);
		self->context->VSSetShader(h->program.dx_vertex_shader, NULL, 0);
		self->context->PSSetShader(h->program.dx_pixel_shader, NULL, 0);

		self->state.program = h;
	}

	inline static void
	_cmd_buffer_map_write_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* h = (Handle*)cmd_pop_ptr(cmd, ix);
		Block block = cmd_pop_block(cmd, ix);

		assert(h->buffer.usage == USAGE::DYNAMIC);
		assert(block.size == h->buffer.size &&
			   "cmd_buffer_map_write require the block to be the same size as the buffer");

		D3D11_MAPPED_SUBRESOURCE resource{};
		HRESULT res = self->context->Map(h->buffer.dx, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
		assert(SUCCEEDED(res) && "ID3D11DeviceContext::Map failed");

		::memcpy(resource.pData, block.ptr, block.size);

		self->context->Unmap(h->buffer.dx, 0);
	}

	inline static void
	_cmd_geometry_draw_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		assert(self->state.pipeline && "you must use a pipeline before geometry draw command");
		assert(self->state.program && "you must use a program before geometry draw command");

		Handle* h = (Handle*)cmd_pop_ptr(cmd, ix);
		PRIMITIVE primitive = PRIMITIVE(cmd_pop8(cmd, ix));
		uint32_t vertex_count = cmd_pop32(cmd, ix);

		assert(h->kind == Handle::KIND_GEOMETRY);

		Input_Layout& input_layout = self->state.program->program.layout;
		assert(buffer_layout_compatible(h->geometry.layout, input_layout) &&
			   "incompatible geometry buffer layout with the current used program input layout");

		switch(primitive)
		{
			case PRIMITIVE::POINTS:
				self->context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
				break;

			case PRIMITIVE::LINES:
				self->context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
				break;

			case PRIMITIVE::TRIANGLES:
				self->context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				break;

			default:
				assert(false && "unreachable");
				break;
		}

		for(size_t i = 0; i < h->geometry.layout.count; ++i)
		{
			Input_Buffer& input_buffer = h->geometry.layout[i];
			Handle* buffer_handle = (Handle*)input_buffer.buffer;
			UINT stride = input_buffer.stride;
			UINT offset = input_buffer.offset;
			self->context->IASetVertexBuffers(i, 1, &buffer_handle->buffer.dx, &stride, &offset);
		}
		self->context->Draw(vertex_count, 0);
	}

	inline static void
	_cmd_uniform_bind_run(Graphics* self, const Cmd& cmd, size_t& ix)
	{
		Handle* h = (Handle*)cmd_pop_ptr(cmd, ix);
		assert(h->kind == Handle::KIND_BUFFER && h->buffer.type == BUFFER_TYPE::UNIFORM &&
			"handle is not a uniform buffer");
		SHADER_TYPE type = (SHADER_TYPE)cmd_pop8(cmd, ix);
		uint32_t slot = cmd_pop32(cmd, ix);

		switch(type)
		{
			case SHADER_TYPE::VERTEX:
				self->context->VSSetConstantBuffers(slot, 1, &h->buffer.dx);
				break;

			case SHADER_TYPE::PIXEL:
				self->context->PSSetConstantBuffers(slot, 1, &h->buffer.dx);
				break;

			default:
				assert(false && "unreachable");
				break;
		}
	}

	inline static void
	_cmd_run(Graphics* self, const Cmd& cmd)
	{
		size_t ix = 0;
		while(ix < cmd.bytes.count)
		{
			ISA opcode = cmd_pop_opcode(cmd, ix);
			switch(opcode)
			{
				case ISA::WINDOW_USE:
					_cmd_window_use_run(self, cmd, ix);
					break;

				case ISA::WINDOW_PRESENT:
					_cmd_window_present_run(self, cmd, ix);
					break;

				case ISA::PIPELINE_USE:
					_cmd_pipeline_use_run(self, cmd, ix);
					break;

				case ISA::CLEAR:
					_cmd_clear_run(self, cmd, ix);
					break;

				case ISA::CLEAR_COLOR:
					_cmd_clear_color_run(self, cmd, ix);
					break;

				case ISA::CLEAR_DEPTH_STENCIL:
					_cmd_clear_depth_stencil_run(self, cmd, ix);
					break;

				case ISA::PROGRAM_USE:
					_cmd_program_use_run(self, cmd, ix);
					break;

				case ISA::BUFFER_MAP_WRITE:
					_cmd_buffer_map_write_run(self, cmd, ix);
					break;

				case ISA::GEOMETRY_DRAW:
					_cmd_geometry_draw_run(self, cmd, ix);
					break;

				case ISA::IGL:
				default:
					assert(false && "invalid instruction");
					break;
			}
		}
	}

	inline static void
	_cmd_process_all(Graphics* self)
	{
		mutex_lock(self->cmd_buf_mtx);
			std::swap(self->cmd_buf_private, self->cmd_buf_public);
		mutex_unlock(self->cmd_buf_mtx);

		std::sort(buf_begin(self->cmd_buf_private), buf_end(self->cmd_buf_private),
				  [](const Cmd& a, const Cmd& b) { return a.sort_key < b.sort_key; });

		for(Cmd& cmd : self->cmd_buf_private)
		{
			_cmd_run(self, cmd);
			self->cmd_discard(cmd);
		}

		//clear the buffer
		buf_clear(self->cmd_buf_private);
	}

	inline static void
	_reset_state(Graphics* self)
	{
		self->state = State{};

		self->context->OMSetRenderTargets(0, nullptr, nullptr);
		self->context->OMSetDepthStencilState(nullptr, 0);
		self->context->RSSetState(nullptr);
		self->context->OMSetBlendState(nullptr, nullptr, 0);
	}


	void
	Graphics::init()
	{
		this->handle_pool = pool_new(sizeof(Handle), 1024);
		this->handle_pool_mtx = mutex_new("DX11 Handle Pool Mutex");

		this->cmd_reuse = buf_new<Cmd>();
		this->cmd_reuse_mtx = mutex_new("DX11 Cmd Reuse Mutex");

		this->cmd_buf_public = buf_new<Cmd>();
		this->cmd_buf_private = buf_new<Cmd>();
		this->cmd_buf_mtx = mutex_new("DX11 Command Buffer Mutex");

		this->req_public = buf_new<Request>();
		this->req_private = buf_new<Request>();
		this->req_mtx = mutex_new("DX11 Requests Mutex");

		this->state = State{};

		//create dxgi factory
		HRESULT res = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&this->factory);
		assert(SUCCEEDED(res) && "CreateDXGIFactory failed");

		//get the first adapter to the primary GPU
		res = factory->EnumAdapters(0, &this->adapter);
		assert(SUCCEEDED(res) && "IDXGIFactory::EnumAdapters failed");

		UINT creation_flags = 0;
		//make sure you have Windows SDK installed for the below flags to work
		#if DEBUG
			//creation_flags = D3D11_CREATE_DEVICE_DEBUG | //enables debug checks layer
			//				 D3D11_CREATE_DEVICE_DEBUGGABLE; //enables shader debugging
		#endif

		//directX will attempt to create the following devices in order
		//if one fails it will try the next
		const D3D_FEATURE_LEVEL feature_levels[] = {
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0
		};

		//1. Create the D3D11 Device and Device Context
		res = D3D11CreateDevice(nullptr, //DXGIAdapter set to null to use the default GPU
								D3D_DRIVER_TYPE_HARDWARE, //choose hardware accelrated driver
								nullptr, //we don't have software rasterizer
								creation_flags,
								feature_levels, //feature levels pointer
								2, //feature levels count
								D3D11_SDK_VERSION,
								&this->device,
								nullptr, //the choosen feature level from the above list we ignore this output
								&this->context);
		assert(SUCCEEDED(res) && "D3D11CreateDevice Failed");
	}

	void
	Graphics::dispose()
	{
		pool_free(this->handle_pool);
		mutex_free(this->handle_pool_mtx);

		destruct(this->cmd_reuse);
		mutex_free(this->cmd_reuse_mtx);

		buf_free(this->cmd_buf_public);
		buf_free(this->cmd_buf_private);
		mutex_free(this->cmd_buf_mtx);

		buf_free(this->req_public);
		buf_free(this->req_private);
		mutex_free(this->req_mtx);

		this->context->Release();
		this->context = nullptr;

		this->device->Release();
		this->device = nullptr;

		this->adapter->Release();
		this->adapter = nullptr;

		this->factory->Release();
		this->factory = nullptr;
	}

	void
	Graphics::window_init(Window handle)
	{
		_request_push(this, _request_window_init(handle));
	}

	void
	Graphics::window_dispose(Window handle)
	{
		_request_push(this, _request_window_dispose(handle));
	}


	//Cmd
	Cmd
	Graphics::cmd_new()
	{
		Cmd res = platform::cmd_new();
		mutex_lock(this->cmd_reuse_mtx);
			if(this->cmd_reuse.count > 0)
			{
				res = buf_top(this->cmd_reuse);
				buf_pop(this->cmd_reuse);
			}
		mutex_unlock(this->cmd_reuse_mtx);
		return res;
	}

	void
	Graphics::cmd_discard(Cmd& cmd)
	{
		cmd_clear_bytes(cmd);
		mutex_lock(this->cmd_reuse_mtx);
			buf_push(this->cmd_reuse, cmd);
		mutex_unlock(this->cmd_reuse_mtx);
		cmd = Cmd{};
	}

	void
	Graphics::cmd_submit(Cmd& cmd, int sort_key)
	{
		cmd.sort_key = sort_key;
		mutex_lock(this->cmd_buf_mtx);
			buf_push(this->cmd_buf_public, cmd);
		mutex_unlock(this->cmd_buf_mtx);
		cmd = Cmd{};
	}

	void
	Graphics::cmd_flush()
	{
		_request_process_all(this);
		_cmd_process_all(this);
		#if DEBUG
			_reset_state(this);
		#endif
	}


	//Pipeline
	Pipeline
	Graphics::pipeline_new(const Input_Layout& layout)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_PIPELINE;

		h->pipeline.clear_color = Color{};
		h->pipeline.layout = layout;

		_request_push(this, _request_pipeline_init(h));

		return (Pipeline)h;
	}

	void
	Graphics::pipeline_free(Pipeline pipeline)
	{
		Handle* h = (Handle*)pipeline;
		assert(h->kind == Handle::KIND_PIPELINE);
		_request_push(this, _request_pipeline_dispose(h));
	}

	void
	Graphics::pipeline_clearcolor(Pipeline pipeline, const Color& c)
	{
		Handle* h = (Handle*)pipeline;
		assert(h->kind == Handle::KIND_PIPELINE);
		h->pipeline.clear_color = c;
	}


	//Buffer
	Buffer
	Graphics::buffer_new(BUFFER_TYPE type, USAGE usage, const mn::Block& data)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_BUFFER;
		h->buffer.type = type;
		h->buffer.usage = usage;

		_request_push(this, _request_buffer_init(h, data));
		return (Buffer)h;
	}

	void
	Graphics::buffer_free(Buffer buffer)
	{
		_request_push(this, _request_buffer_dispose((Handle*)buffer));
	}


	//Texture2D
	Texture2D
	Graphics::texture2d_new(uint32_t width, uint32_t height,
							const Block& data, USAGE usage, PIXEL_FORMAT format)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_BUFFER;
		h->texture2d.width = width;
		h->texture2d.height = height;
		h->texture2d.usage = usage;
		h->texture2d.format = format;

		_request_push(this, _request_texture2d_init(h, width, height, data, usage, format));
		return (Texture2D)h;
	}

	void
	Graphics::texture2d_free(Texture2D texture)
	{
		_request_push(this, _request_texture2d_dispose((Handle*)texture));
	}


	//Sampler
	Sampler
	Graphics::sampler_new(FILTER filter, ADDRESS_MODE u, ADDRESS_MODE v, ADDRESS_MODE w, const Color& border)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_SAMPLER;
		h->sampler.filter = filter;
		h->sampler.u = u;
		h->sampler.v = v;
		h->sampler.w = w;
		h->sampler.border = border;

		_request_push(this, _request_sampler_init(h, filter, u, v, w, border));
		return (Sampler)h;
	}

	void
	Graphics::sampler_free(Sampler sampler)
	{
		_request_push(this, _request_sampler_dispose((Handle*)sampler));
	}


	//Program
	Program
	Graphics::program_new(const Input_Layout& layout, const Str& vs, const Str& ps)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_PROGRAM;
		h->program.layout = layout;

		_request_push(this, _request_program_init(h, layout, vs, ps));
		return (Program)h;
	}

	void
	Graphics::program_free(Program program)
	{
		_request_push(this, _request_program_dispose((Handle*)program));
	}


	//Geometry
	Geometry
	Graphics::geometry_new(const Buffer_Layout& layout)
	{
		Handle* h = _handle_new(this);
		h->kind = Handle::KIND_GEOMETRY;
		h->geometry.layout = layout;
		return (Geometry)h;
	}

	void
	Graphics::geometry_free(Geometry geometry)
	{
		_request_push(this, _request_goemetry_dispose((Handle*)geometry));
	}
}
