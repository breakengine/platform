project "glew"
	language "C++"
	kind "SharedLib"

	files
	{
		"include/**.h",
		"src/**.c"
	}

	includedirs "include"

	--language configuration
	warnings "Extra"
	cppdialect "c++17"
	systemversion "latest"

	--linux configuration
	filter "system:linux"
		defines { "OS_LINUX" }
		links { "X11", "GL", "GLU" }

	--windows configuration
	filter "system:windows"
		defines { "OS_WINDOWS" }
		links { "opengl32" }

	--os agnostic configuration
	filter "configurations:debug"
		targetsuffix "d"
		defines {"DEBUG", "GLEW_BUILD"}
		symbols "On"

	filter "configurations:release"
		defines {"NDEBUG", "GLEW_BUILD"}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"