#if OS_LINUX

#include <assert.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include <GL/glx.h>

#include <mn/Memory.h>

#include <hml/Vector.h>

#include "break/platform-hal/Event.h"
#include "break/platform-hal/Window.h"

#include "break/platform-hal/linux/Window.h"

namespace brk::platform::linux
{
	Atom*
	_wm_delete_window(Display* display)
	{
		static Atom atom = XInternAtom(display, "WM_DELETE_WINDOW", False);
		return &atom;
	}
}

namespace brk::platform
{
	using hml::vec2i;
	using mn::alloc;
	using mn::free;


	inline static Atom*
	_wm_protocols(Display* display)
	{
		static Atom atom = XInternAtom(display, "WM_PROTOCOLS", False);
		return &atom;
	}

	inline static KEYBOARD
	_map_keyboard_key(KeySym k)
	{
		switch(k)
		{
			//letters
			case XK_a:		return KEYBOARD::A;
			case XK_b:		return KEYBOARD::B;
			case XK_c:		return KEYBOARD::C;
			case XK_d:		return KEYBOARD::D;
			case XK_e:		return KEYBOARD::E;
			case XK_f:		return KEYBOARD::F;
			case XK_g:		return KEYBOARD::G;
			case XK_h:		return KEYBOARD::H;
			case XK_i:		return KEYBOARD::I;
			case XK_j:		return KEYBOARD::J;
			case XK_k:		return KEYBOARD::K;
			case XK_l:		return KEYBOARD::L;
			case XK_m:		return KEYBOARD::M;
			case XK_n:		return KEYBOARD::N;
			case XK_o:		return KEYBOARD::O;
			case XK_p:		return KEYBOARD::P;
			case XK_q:		return KEYBOARD::Q;
			case XK_r:		return KEYBOARD::R;
			case XK_s:		return KEYBOARD::S;
			case XK_t:		return KEYBOARD::T;
			case XK_u:		return KEYBOARD::U;
			case XK_v:		return KEYBOARD::V;
			case XK_w:		return KEYBOARD::W;
			case XK_x:		return KEYBOARD::X;
			case XK_y:		return KEYBOARD::Y;
			case XK_z:		return KEYBOARD::Z;

			//numbers
			case XK_0:		return KEYBOARD::NUM_0;
			case XK_1:		return KEYBOARD::NUM_1;
			case XK_2:		return KEYBOARD::NUM_2;
			case XK_3:		return KEYBOARD::NUM_3;
			case XK_4:		return KEYBOARD::NUM_4;
			case XK_5:		return KEYBOARD::NUM_5;
			case XK_6:		return KEYBOARD::NUM_6;
			case XK_7:		return KEYBOARD::NUM_7;
			case XK_8:		return KEYBOARD::NUM_8;
			case XK_9:		return KEYBOARD::NUM_9;

			//misc
			case XK_Escape:		return KEYBOARD::ESC;
			case XK_minus:		return KEYBOARD::MINUS;
			case XK_comma:		return KEYBOARD::COMMA;
			case XK_equal:		return KEYBOARD::EQUAL;
			case XK_period:		return KEYBOARD::PERIOD;
			case XK_space:		return KEYBOARD::SPACE;
			case XK_Return:		return KEYBOARD::ENTER;
			case XK_BackSpace:	return KEYBOARD::BACKSPACE;
			case XK_Tab:		return KEYBOARD::TAB;
			case XK_slash:		return KEYBOARD::FORWARD_SLASH;
			case XK_backslash:	return KEYBOARD::BACKSLASH;
			case XK_Delete:		return KEYBOARD::DELETE;
			case XK_Shift_L:	return KEYBOARD::LEFT_SHIFT;
			case XK_Shift_R:	return KEYBOARD::RIGHT_SHIFT;
			case XK_Alt_L:		return KEYBOARD::LEFT_ALT;
			case XK_Alt_R:		return KEYBOARD::RIGHT_ALT;
			case XK_Control_L:	return KEYBOARD::LEFT_CTRL;
			case XK_Control_R:	return KEYBOARD::RIGHT_CTRL;
			case XK_End:		return KEYBOARD::END;
			case XK_Home:		return KEYBOARD::HOME;
			case XK_Insert:		return KEYBOARD::INSERT;
			case XK_Page_Up:	return KEYBOARD::PAGEUP;
			case XK_Page_Down:	return KEYBOARD::PAGEDOWN;
			case XK_Left:		return KEYBOARD::LEFT;
			case XK_Up:			return KEYBOARD::UP;
			case XK_Right:		return KEYBOARD::RIGHT;
			case XK_Down:		return KEYBOARD::DOWN;
			case XK_F1:			return KEYBOARD::F1;
			case XK_F2:			return KEYBOARD::F2;
			case XK_F3:			return KEYBOARD::F3;
			case XK_F4:			return KEYBOARD::F4;
			case XK_F5:			return KEYBOARD::F5;
			case XK_F6:			return KEYBOARD::F6;
			case XK_F7:			return KEYBOARD::F7;
			case XK_F8:			return KEYBOARD::F8;
			case XK_F9:			return KEYBOARD::F9;
			case XK_F10:		return KEYBOARD::F10;
			case XK_F11:		return KEYBOARD::F11;
			case XK_F12:		return KEYBOARD::F12;
			default:			return KEYBOARD::COUNT;
		}
	}

	inline static MOUSE
	_map_mouse_button(int button)
	{
		switch(button)
		{
			case Button1:	return MOUSE::LEFT;
			case Button2:	return MOUSE::MIDDLE;
			case Button3:	return MOUSE::RIGHT;
			default: 		return MOUSE::COUNT;
		}
	}

	Window
	window_new(int width, int height, const char* title)
	{
		linux::Window* self = alloc<linux::Window>();
		self->width = width;
		self->height = height;
		self->title = title;
		self->running = true;

		return (Window)self;
	}

	void
	window_free(Window window)
	{
		linux::Window* self = (linux::Window*)window;
		self->running = false;
		self->inited = false;
		XCloseDisplay(self->display);
		free(self);
	}

	Window_Event
	window_poll(Window window)
	{
		linux::Window* self = (linux::Window*)window;

		XEvent event{};
		self->event = Window_Event{};

		if(self->inited == false)
			return self->event;

		if(XPending(self->display))
		{
			XNextEvent(self->display, &event);
			if(XFilterEvent(&event, None))
				return self->event;

			switch(event.type)
			{
				case ClientMessage:
				{
					if(event.xclient.message_type == *_wm_protocols(self->display) &&
					   (Atom)event.xclient.data.l[0] == *linux::_wm_delete_window(self->display))
					{
						self->running = false;
						self->event = window_event_close();
					}
				}
				break;

				case KeyPress:
				{
					self->event = window_event_keyboard_key(
						_map_keyboard_key(XLookupKeysym(&event.xkey, 0)), KEY_STATE::DOWN);
				}
				break;

				case KeyRelease:
				{
					self->event = window_event_keyboard_key(
						_map_keyboard_key(XLookupKeysym(&event.xkey, 0)), KEY_STATE::UP);
				}
				break;

				case ButtonPress:
				{
					self->event = window_event_mouse_button(
						_map_mouse_button(event.xbutton.button), KEY_STATE::DOWN);
				}
				break;

				case ButtonRelease:
				{
					self->event = window_event_mouse_button(
						_map_mouse_button(event.xbutton.button), KEY_STATE::UP);
				}
				break;

				case MotionNotify:
				{
					self->event = window_event_mouse_move(event.xbutton.x, event.xbutton.y);
				}
				break;

				case ConfigureNotify:
				{
					if (self->width != event.xconfigure.width ||
						self->height != event.xconfigure.height)
					{
						self->width = event.xconfigure.width;
						self->height = event.xconfigure.height;
						self->event = window_event_resize(self->width, self->height);
					}
				}
				break;

				default:
					break;
			}
		}
		return self->event;
	}

	void
	window_swap_buffers(Window window)
	{
		linux::Window* self = (linux::Window*)window;
		glXSwapBuffers(self->display, self->handle);
	}

	vec2i
	window_size(Window window)
	{
		linux::Window* self = (linux::Window*)window;
		return vec2i{ self->width, self->height };
	}

	void
	window_user_data_set(Window window, void* ptr)
	{
		linux::Window* self = (linux::Window*)window;
		self->user_data = ptr;
	}

	void*
	window_user_data(Window window)
	{
		linux::Window* self = (linux::Window*)window;
		return self->user_data;
	}
}

#endif