#pragma once

namespace brk::platform::winos
{
	struct Window
	{
		int width, height;
		const char* title;
		HWND handle;
		HDC hdc;
		Window_Event event;
		bool running;
		void* user_data;
	};
}
