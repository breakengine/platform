#pragma once

#include <hml/Vector.h>

#include <mn/Base.h>

#include <stdint.h>
#include <assert.h>

#include <initializer_list>

namespace brk::platform
{
	enum class ISA : uint8_t
	{
		//Illegal instruction
		IGL,

		//[WINDOW_USE, Window]
		WINDOW_USE,

		//[WINDOW_PRESENT] this uses the currently used window
		WINDOW_PRESENT,

		//[PIPELINE_USE, Pipeline]
		PIPELINE_USE,

		//[CLEAR, CLEAR_FLAGS]
		CLEAR,

		//[CLEAR_COLOR, Color]
		CLEAR_COLOR,

		//[CLEAR_DEPTH_STENCIL, float(Depth), uint8_t(Stencil)]
		CLEAR_DEPTH_STENCIL,

		//[PROGRAM_USE, Program]
		PROGRAM_USE,

		//[BUFFER_MAP_WRITE, Buffer, Block]
		BUFFER_MAP_WRITE,

		//[GEOMETRY_DRAW, Geometry, PRIMITIVE, uint32_t(vertex_count)]
		GEOMETRY_DRAW,

		//[UNIFORM_BIND, Buffer(uniform buffer), SHADER_TYPE, uint32_t(slot)]
		UNIFORM_BIND
	};

	using Color = hml::vec4f;

	enum CLEAR_FLAGS : uint8_t
	{
		CLEAR_FLAGS_COLOR = 1 << 0,
		CLEAR_FLAGS_DEPTH = 1 << 1
	};

	enum class BUFFER_TYPE : uint8_t
	{
		VERTEX,
		INDEX,
		UNIFORM
	};

	enum class USAGE : uint8_t
	{
		STATIC,
		DYNAMIC
	};

	enum class PRIMITIVE: uint8_t
	{
		POINTS,
		LINES,
		TRIANGLES
	};

	enum class SHADER_TYPE: uint8_t
	{
		VERTEX,
		PIXEL
	};

	enum class PIXEL_FORMAT : uint8_t
	{
		RGBA,
		RGB
	};

	enum class FILTER: uint8_t
	{
		LINEAR,
		POINT
	};

	enum class ADDRESS_MODE: uint8_t
	{
		WRAP,
		CLAMP,
		MIRROR,
		BORDER
	};

	enum class INPUT_TYPE: uint8_t
	{
		VEC3F,
		RGBA
	};

	inline static uint32_t
	input_type_element_count(INPUT_TYPE type)
	{
		switch(type)
		{
			case INPUT_TYPE::VEC3F:
				return 3;

			case INPUT_TYPE::RGBA:
				return 4;

			default:
				assert(false && "unreachable");
				return 0;
		}
	}

	inline static uint32_t
	input_type_size(INPUT_TYPE type)
	{
		switch(type)
		{
			case INPUT_TYPE::VEC3F:
				return 12;

			case INPUT_TYPE::RGBA:
				return 16;

			default:
				assert(false && "unreachable");
				return 0;
		}
	}

	enum class SEMANTIC: uint8_t
	{
		POSITION,
		COLOR
	};

	struct Input_Element
	{
		SEMANTIC semantic;
		INPUT_TYPE type;

		bool
		operator==(const Input_Element& other) const
		{
			return semantic == other.semantic && type == other.type;
		}

		bool
		operator!=(const Input_Element& other) const
		{
			return !operator==(other);
		}
	};

	struct Input_Layout
	{
		constexpr static size_t MAX_ELEMENTS = 5;

		Input_Element elements[MAX_ELEMENTS];
		size_t count;

		Input_Element&
		operator[](size_t ix)
		{
			assert(count > ix);
			return elements[ix];
		}

		const Input_Element&
		operator[](size_t ix) const
		{
			assert(count > ix);
			return elements[ix];
		}

		bool
		operator==(const Input_Layout& other) const
		{
			if(count != other.count)
				return false;
			for(size_t i = 0; i < count; ++i)
				if(elements[i] != other.elements[i])
					return false;
			return true;
		}

		bool
		operator!=(const Input_Layout& other) const
		{
			return !operator==(other);
		}
	};

	inline static Input_Layout
	input_layout_new(std::initializer_list<Input_Element> elements)
	{
		assert(elements.size() < Input_Layout::MAX_ELEMENTS);
		Input_Layout self{};
		for(const auto& element: elements)
			self[self.count++] = element;
		return self;
	}


	//Buffer Layout
	MS_FWD_HANDLE(Buffer);

	struct Input_Buffer
	{
		Buffer buffer;
		INPUT_TYPE type;
		uint32_t element_count;
		uint32_t stride;
		uint32_t offset;
		bool normalized;
	};

	inline static Input_Buffer
	input_buffer_new(Buffer buffer, INPUT_TYPE type)
	{
		Input_Buffer self{};
		self.buffer = buffer;
		self.type = type;
		self.element_count = input_type_element_count(type);
		self.stride = input_type_size(type);
		self.offset = 0;
		self.normalized = false;
		return self;
	}

	struct Buffer_Layout
	{
		Input_Buffer buffers[Input_Layout::MAX_ELEMENTS];
		size_t count;

		Input_Buffer&
		operator[](size_t ix)
		{
			assert(count > ix);
			return buffers[ix];
		}

		const Input_Buffer&
		operator[](size_t ix) const
		{
			assert(count > ix);
			return buffers[ix];
		}
	};

	inline static Buffer_Layout
	buffer_layout_new(std::initializer_list<Input_Buffer> buffers)
	{
		assert(buffers.size() < Input_Layout::MAX_ELEMENTS);
		Buffer_Layout self{};
		for(const auto& buffer: buffers)
			self[self.count++] = buffer;
		return self;
	}

	inline static bool
	buffer_layout_compatible(const Buffer_Layout& self, const Input_Layout& layout)
	{
		if(self.count != layout.count)
			return false;

		for(size_t i = 0; i < self.count; ++i)
			if(self[i].type != layout[i].type)
				return false;

		return true;
	}
}
