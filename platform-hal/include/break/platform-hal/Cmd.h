#pragma once

#include "break/platform-hal/ISA.h"

#include <mn/Base.h>
#include <mn/Buf.h>

#include <stdint.h>
#include <stddef.h>

namespace brk::platform
{
	struct Cmd
	{
		mn::Buf<uint8_t> bytes;
		int sort_key;
	};

	inline static Cmd
	cmd_new()
	{
		Cmd self{};
		self.bytes = mn::buf_new<uint8_t>();
		return self;
	}

	inline static void
	cmd_free(Cmd& self)
	{
		mn::buf_free(self.bytes);
	}

	inline static void
	destruct(Cmd& self)
	{
		cmd_free(self);
	}

	inline static void
	cmd_clear_bytes(Cmd& self)
	{
		buf_clear(self.bytes);
	}


	//push
	inline static void
	cmd_push8(Cmd& self, uint8_t value)
	{
		mn::buf_push(self.bytes, value);
	}

	inline static void
	cmd_push16(Cmd& self, uint16_t value)
	{
		mn::buf_push(self.bytes, uint8_t(value));
		mn::buf_push(self.bytes, uint8_t(value >> 8));
	}

	inline static void
	cmd_push32(Cmd& self, uint32_t value)
	{
		mn::buf_push(self.bytes, uint8_t(value));
		mn::buf_push(self.bytes, uint8_t(value >> 8));
		mn::buf_push(self.bytes, uint8_t(value >> 16));
		mn::buf_push(self.bytes, uint8_t(value >> 24));
	}

	inline static void
	cmd_push32f(Cmd& self, float value)
	{
		cmd_push32(self, *(uint32_t*)&value);
	}

	inline static void
	cmd_push64(Cmd& self, uint64_t value)
	{
		mn::buf_push(self.bytes, uint8_t(value));
		mn::buf_push(self.bytes, uint8_t(value >> 8));
		mn::buf_push(self.bytes, uint8_t(value >> 16));
		mn::buf_push(self.bytes, uint8_t(value >> 24));
		mn::buf_push(self.bytes, uint8_t(value >> 32));
		mn::buf_push(self.bytes, uint8_t(value >> 40));
		mn::buf_push(self.bytes, uint8_t(value >> 48));
		mn::buf_push(self.bytes, uint8_t(value >> 56));
	}

	inline static void
	cmd_push64f(Cmd& self, double value)
	{
		cmd_push64(self, *(uint64_t*)&value);
	}

	inline static void
	cmd_push_ins(Cmd& self, ISA opcode)
	{
		mn::buf_push(self.bytes, uint8_t(opcode));
	}

	inline static void
	cmd_push_ptr(Cmd& self, void* ptr)
	{
		cmd_push64(self, uint64_t(ptr));
	}

	inline static void
	cmd_push_color(Cmd& self, const Color& color)
	{
		cmd_push32f(self, color.x);
		cmd_push32f(self, color.y);
		cmd_push32f(self, color.z);
		cmd_push32f(self, color.w);
	}

	inline static void
	cmd_push_block(Cmd& self, const mn::Block& block)
	{
		cmd_push_ptr(self, block.ptr);
		cmd_push64(self, block.size);
	}


	//pop
	inline static uint8_t
	cmd_pop8(const Cmd& self, size_t& ix)
	{
		return self.bytes[ix++];
	}

	inline static uint16_t
	cmd_pop16(const Cmd& self, size_t& ix)
	{
		assert((ix + sizeof(uint16_t)) <= self.bytes.count && "index out of range");
		uint16_t res = *(uint16_t*)(self.bytes.ptr + ix);
		ix += sizeof(uint16_t);
		return res;
	}

	inline static uint32_t
	cmd_pop32(const Cmd& self, size_t& ix)
	{
		assert((ix + sizeof(uint32_t)) <= self.bytes.count && "index out of range");
		uint32_t res = *(uint32_t*)(self.bytes.ptr + ix);
		ix += sizeof(uint32_t);
		return res;
	}

	inline static float
	cmd_pop32f(const Cmd& self, size_t& ix)
	{
		uint32_t value = cmd_pop32(self, ix);
		return *(float*)&value;
	}

	inline static uint64_t
	cmd_pop64(const Cmd& self, size_t& ix)
	{
		assert((ix + sizeof(uint64_t)) <= self.bytes.count && "index out of range");
		uint64_t res = *(uint64_t*)(self.bytes.ptr + ix);
		ix += sizeof(uint64_t);
		return res;
	}

	inline static double
	cmd_pop64f(const Cmd& self, size_t& ix)
	{
		uint64_t value = cmd_pop64(self, ix);
		return *(double*)&value;
	}

	inline static ISA
	cmd_pop_opcode(const Cmd& self, size_t& ix)
	{
		return (ISA)cmd_pop8(self, ix);
	}

	inline static void*
	cmd_pop_ptr(const Cmd& self, size_t& ix)
	{
		return (void*)cmd_pop64(self, ix);
	}

	inline static Color
	cmd_pop_color(const Cmd& self, size_t& ix)
	{
		Color c{};
		c.x = cmd_pop32f(self, ix);
		c.y = cmd_pop32f(self, ix);
		c.z = cmd_pop32f(self, ix);
		c.w = cmd_pop32f(self, ix);
		return c;
	}

	inline static mn::Block
	cmd_pop_block(const Cmd& self, size_t& ix)
	{
		mn::Block block{};
		block.ptr = cmd_pop_ptr(self, ix);
		block.size = cmd_pop64(self, ix);
		return block;
	}


	//instruction interface
	MS_FWD_HANDLE(Pipeline);
	MS_FWD_HANDLE(Window);
	MS_FWD_HANDLE(Buffer);
	MS_FWD_HANDLE(Program);
	MS_FWD_HANDLE(Geometry);

	inline static void
	cmd_window_use(Cmd& self, Window window)
	{
		cmd_push_ins(self, ISA::WINDOW_USE);
		cmd_push_ptr(self, window);
	}

	inline static void
	cmd_present(Cmd& self)
	{
		cmd_push_ins(self, ISA::WINDOW_PRESENT);
	}

	inline static void
	cmd_pipeline_use(Cmd& self, Pipeline pipeline)
	{
		cmd_push_ins(self, ISA::PIPELINE_USE);
		cmd_push_ptr(self, pipeline);
	}

	inline static void
	cmd_clear(Cmd& self, uint8_t flags)
	{
		cmd_push_ins(self, ISA::CLEAR);
		cmd_push8(self, flags);
	}

	inline static void
	cmd_clear_color(Cmd& self, const Color& color)
	{
		cmd_push_ins(self, ISA::CLEAR_COLOR);
		cmd_push_color(self, color);
	}

	inline static void
	cmd_clear_depth_stencil(Cmd& self, float depth, uint8_t stencil)
	{
		cmd_push_ins(self, ISA::CLEAR_DEPTH_STENCIL);
		cmd_push32f(self, depth);
		cmd_push8(self, stencil);
	}

	inline static void
	cmd_program_use(Cmd& self, Program program)
	{
		cmd_push_ins(self, ISA::PROGRAM_USE);
		cmd_push_ptr(self, program);
	}

	inline static void
	cmd_buffer_map_write(Cmd& self, Buffer buffer, const mn::Block& data)
	{
		cmd_push_ins(self, ISA::BUFFER_MAP_WRITE);
		cmd_push_ptr(self, buffer);
		cmd_push_block(self, data);
	}

	inline static void
	cmd_geometry_draw(Cmd& self, Geometry geometry, PRIMITIVE primitive, uint32_t vertex_count)
	{
		cmd_push_ins(self, ISA::GEOMETRY_DRAW);
		cmd_push_ptr(self, geometry);
		cmd_push8(self, uint8_t(primitive));
		cmd_push32(self, vertex_count);
	}

	inline static void
	cmd_uniform_bind(Cmd& self, Buffer buffer, SHADER_TYPE shader_type, uint32_t slot)
	{
		cmd_push_ins(self, ISA::UNIFORM_BIND);
		cmd_push8(self, uint8_t(shader_type));
		cmd_push_ptr(self, buffer);
		cmd_push32(self, slot);
	}
}
