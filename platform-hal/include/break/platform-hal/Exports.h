#pragma once

#if defined(OS_WINDOWS)
	#if defined(PLT_HAL_DLL)
		#define API_PLT_HAL __declspec(dllexport)
	#else
		#define API_PLT_HAL __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_PLT_HAL 
#endif