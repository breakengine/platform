#pragma once

namespace brk::platform
{
	enum class KEYBOARD
	{
		BACKSPACE,
		TAB,
		ENTER,
		ESC,
		SPACE,
		MINUS,
		PERIOD,
		COMMA,
		NUM_0,
		NUM_1,
		NUM_2,
		NUM_3,
		NUM_4,
		NUM_5,
		NUM_6,
		NUM_7,
		NUM_8,
		NUM_9,
		COLON,
		SEMICOLON,
		EQUAL,
		FORWARD_SLASH,
		LEFT_BRACKET,
		RIGHT_BRACKET,
		BACKSLASH,
		BACKQUOTE,
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,
		DELETE,
		NUMPAD_0,
		NUMPAD_1,
		NUMPAD_2,
		NUMPAD_3,
		NUMPAD_4,
		NUMPAD_5,
		NUMPAD_6,
		NUMPAD_7,
		NUMPAD_8,
		NUMPAD_9,
		NUMPAD_PERIOD,
		NUMPAD_DIVIDE,
		NUMPAD_PLUS,
		NUMPAD_MINUS,
		NUMPAD_ENTER,
		NUMPAD_EQUALS,
		NUMPAD_MULTIPLY,
		UP,
		DOWN,
		RIGHT,
		LEFT,
		INSERT,
		HOME,
		END,
		PAGEUP,
		PAGEDOWN,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,
		NUM_LOCK,
		CAPS_LOCK,
		SCROLL_LOCK,
		RIGHT_SHIFT,
		LEFT_SHIFT,
		RIGHT_CTRL,
		LEFT_CTRL,
		LEFT_ALT,
		RIGHT_ALT,
		LEFT_META,
		RIGHT_META,
		COUNT
	};

	enum class MOUSE
	{
		LEFT,
		MIDDLE,
		RIGHT,
		COUNT
	};

	enum class KEY_STATE
	{
		NONE,
		UP,
		DOWN,
		COUNT
	};

	struct Window_Event
	{
		enum KIND
		{
			KIND_NONE,
			KIND_KEYBOARD_KEY,
			KIND_MOUSE_BUTTON,
			KIND_MOUSE_MOVE,
			KIND_WINDOW_RESIZE,
			KIND_WINDOW_CLOSE
		};

		KIND kind;
		union
		{
			struct
			{
				KEYBOARD key;
				KEY_STATE state;
			} keyboard;

			struct
			{
				MOUSE button;
				KEY_STATE state;
			} mouse;

			struct
			{
				int x, y;
			} mouse_move;

			struct
			{
				int width, height;
			} resize;
		};
	};

	inline static Window_Event
	window_event_keyboard_key(KEYBOARD key, KEY_STATE state)
	{
		Window_Event self{};
		self.kind = Window_Event::KIND_KEYBOARD_KEY;
		self.keyboard.key = key;
		self.keyboard.state = state;
		return self;
	}

	inline static Window_Event
	window_event_mouse_button(MOUSE button, KEY_STATE state)
	{
		Window_Event self{};
		self.kind = Window_Event::KIND_MOUSE_BUTTON;
		self.mouse.button = button;
		self.mouse.state = state;
		return self;
	}

	inline static Window_Event
	window_event_mouse_move(int x, int y)
	{
		Window_Event self{};
		self.kind = Window_Event::KIND_MOUSE_MOVE;
		self.mouse_move.x = x;
		self.mouse_move.y = y;
		return self;
	}

	inline static Window_Event
	window_event_resize(int width, int height)
	{
		Window_Event self{};
		self.kind = Window_Event::KIND_WINDOW_RESIZE;
		self.resize.width = width;
		self.resize.height = height;
		return self;
	}

	inline static Window_Event
	window_event_close()
	{
		Window_Event self{};
		self.kind = Window_Event::KIND_WINDOW_CLOSE;
		return self;
	}
}
