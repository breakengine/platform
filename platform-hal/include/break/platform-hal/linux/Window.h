#pragma once

#include "break/platform-hal/Exports.h"

namespace brk::platform::linux
{
	struct Window
	{
		int width, height;
		const char* title;
		::Window handle;
		::Display* display;
		bool running;
		bool inited;
		Window_Event event;
		void* user_data;
	};

	API_PLT_HAL ::Atom*
	_wm_delete_window(::Display* display);
}
