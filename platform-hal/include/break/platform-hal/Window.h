#pragma once

#include "break/platform-hal/Exports.h"
#include "break/platform-hal/Event.h"

#include <hml/Vector.h>

#include <mn/Base.h>

namespace brk::platform
{
	MS_HANDLE(Window);

	API_PLT_HAL Window
	window_new(int width, int height, const char* title);

	API_PLT_HAL void
	window_free(Window window);

	inline static void
	destruct(Window window)
	{
		window_free(window);
	}

	API_PLT_HAL Window_Event
	window_poll(Window window);

	API_PLT_HAL hml::vec2i
	window_size(Window window);

	API_PLT_HAL void
	window_user_data_set(Window window, void* ptr);

	API_PLT_HAL void*
	window_user_data(Window window);
}