#pragma once

#include "break/platform-hal/ISA.h"
#include "break/platform-hal/Cmd.h"

#include <mn/Base.h>
#include <mn/Str.h>

namespace brk::platform
{
	MS_FWD_HANDLE(Window);
	MS_FWD_HANDLE(Pipeline);
	MS_FWD_HANDLE(Buffer);
	MS_FWD_HANDLE(Texture2D);
	MS_FWD_HANDLE(Sampler);
	MS_FWD_HANDLE(Program);
	MS_FWD_HANDLE(Geometry);

	struct IGraphics
	{
		virtual void init() = 0;
		virtual void dispose() = 0;

		virtual void window_init(Window window) = 0;
		virtual void window_dispose(Window window) = 0;

		virtual Cmd cmd_new() = 0;
		virtual void cmd_discard(Cmd& cmd) = 0;
		virtual void cmd_submit(Cmd& cmd, int sort_key) = 0;
		virtual void cmd_flush() = 0;

		virtual Pipeline pipeline_new(const Input_Layout& layout) = 0;
		virtual void pipeline_free(Pipeline pipeline) = 0;
		virtual void pipeline_clearcolor(Pipeline pipeline, const Color& c) = 0;

		virtual Buffer buffer_new(BUFFER_TYPE type, USAGE usage, const mn::Block& data) = 0;
		virtual void buffer_free(Buffer buffer) = 0;

		virtual Texture2D texture2d_new(uint32_t width, uint32_t height,
										const mn::Block& data, USAGE usage, PIXEL_FORMAT format) = 0;
		virtual void texture2d_free(Texture2D texture) = 0;

		virtual Sampler sampler_new(FILTER filter,
									ADDRESS_MODE u,
									ADDRESS_MODE v,
									ADDRESS_MODE w,
									const Color& border) = 0;
		virtual void sampler_free(Sampler handle) = 0;

		virtual Program program_new(const Input_Layout& layout, const mn::Str& vs, const mn::Str& ps) = 0;
		virtual void program_free(Program handle) = 0;

		virtual Geometry geometry_new(const Buffer_Layout& layout) = 0;
		virtual void geometry_free(Geometry geometry) = 0;
	};
}
